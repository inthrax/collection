#!/usr/bin/env perl

use 5.010;
use strict;
use Carp;
use Socket qw(AF_INET AF_UNIX SOCK_STREAM SOCK_DGRAM SOCK_RAW SOL_SOCKET SO_REUSEADDR IPPROTO_TCP TCP_NODELAY SO_TYPE SO_ERROR sockaddr_in);
use Fcntl qw(F_GETFL F_GETFD O_ACCMODE);
use Fcntl grep /^O_/, @Fcntl::EXPORT;
use Devel::Peek;

socket my $fh, AF_INET, SOCK_STREAM, 0 or Carp::croak "listen/socket: $!";
my $s = "xxx";

socket my $fx, AF_INET, SOCK_STREAM, 0 or Carp::croak "listen/socket: $!";
close $fx;

sub so_test($) {
	my $s = shift;
	return undef, "Not a glob: $s" unless ref $s eq 'GLOB';
	my @info = ("$s");
	my $fd = fileno($s);
	return undef, "Not an fd: $s/".(*$s) unless defined $fd;
	push @info, "fd:$fd";
	
	my $fl = fcntl $fh, F_GETFL, 0;
	return undef, "fcntl F_GETFL failed: $! (on @info)" unless defined $fl;
	
	my @flags;
	for (@Fcntl::EXPORT) {
		if (/^O_/) {
			no strict 'refs';
			eval { $_->() } or next;
			#say $_;
			push @flags, $_ if $fl & ($_->());
		}
	}
	push @info, "fl:".join "+",@flags;
	
	my $so;

	$so = getsockopt($fh, SOL_SOCKET, SO_TYPE)
		or return "getsockopt SO_TYPE failed: $! (on @info)";
	$so = unpack "V", $so;
	for (qw(SOCK_STREAM SOCK_RAW SOCK_DGRAM)) {
		no strict 'refs';
		if ($_->() == $so) {
			push @info, $_ if $_->() == $so;
			last;
		}
	}
	$so = getsockopt($fh, SOL_SOCKET, SO_ERROR)
		or return "getsockopt SO_ERROR failed: $! (on @info)";
	$so = unpack "V", $so;
	if ($so) {
		local $! = $so;
		push @info, "$!";
	}
	
	if (my $sa = getpeername($s)) {
		my ($port, $iaddr) = sockaddr_in($sa);
		my $addr = inet_ntoa($iaddr);
		push @info, "$addr:$port";
	}
	else {
		push @info, grep $!{ $_ }, keys %!;
	}
	
	# recv(sock, &buf, 1, MSG_PEEK | MSG_DONTWAIT);
	"@info";
}

for my $fh ($fh, $s, $fx, \*STDIN, \*main) {
	say so_test $fh;
}
