#!/usr/bin/env perl

use strict;
use Benchmark ':all';
use AnyEvent::Util 'fh_nonblocking';

pipe my $r, my $w or die "$!";
#print {$w} "Test\n" or die "$!";
#print <$r>;

my $buf =<<EOB;
GET /123213 HTTP/1.1
Host: localhost:8080
User-Agent: weighttp/0.111
Connection: close

EOB
#print $buf;
fh_nonblocking $r,1;
#fh_nonblocking $w,1;


cmpthese timethese -1, {
	sys => sub {
		syswrite $w, $buf or die "$!";
		sysread $r, my $in, 4096 or die "$!";
	},
	buf => sub {
		syswrite $w, $buf or die "$!";
		read $r, my $in, 4096 or die "$!";
	},
	buf2 => sub {
		syswrite $w, $buf or die "$!";
		my $in;
		my $line;
		readit: {
			if (defined ( $line = <$r> )) {
				$in .= $line;
				return if $line eq "\n";
				redo readit;
			}
		}
	},
	
}