#!/usr/bin/env perl

use strict;
use 5.010;
use Proc::Vsize;

#my $s;
say "AA ".int(vsize($$)/1024);
my @keep;

for (1..100) {
	my $s = "x"x(1024*1024*$_);
	push @keep, \$s;
	say "$_ ".int(vsize($$)/1024);
	#undef $s;
}

say "XX ".int(vsize($$)/1024);

undef @keep;

say "ZZ ".int(vsize($$)/1024);

__END__

sub DESTROY { warn "Destroying @_"; }

sub usemem {
	my $arg = shift || 12 + int( rand(10) );
	my %hash;
	keys(%hash) = 2**( $arg );
	return bless \%hash, 'main';
}

say 'in '.int(vsize($$)/1024);

my $v = usemem(24);
my $sv = 'x'x10240000;
say '26: '.int(vsize($$)/1024);
undef $v;
say 'x: '.int(vsize($$)/1024);
undef $sv;
say 'xx: '.int(vsize($$)/1024);

say int(vsize($$)/1024);
my $hash = usemem(20);
say int(vsize($$)/1024);
undef $hash;
say int(vsize($$)/1024);

$hash = usemem(21);
say int(vsize($$)/1024);
undef $hash;
say int(vsize($$)/1024);

$hash = usemem(20);

#my $var = bless ['x'x1024], 'main';
#my @var = ('x'x1024);

say int(vsize($$)/1024);
undef $hash;
say int(vsize($$)/1024);

#undef $var;
#undef @var;
say int(vsize($$)/1024);

say "before 1st loop ". int(vsize($$)/1024);

for (1..10) {
	my $hash = usemem(14+$_);
	
	say int(vsize($$)/1024);
	
}

say "1st loop done ". int(vsize($$)/1024);

for (1..10) {
	my $hash = usemem();
	
	say int(vsize($$)/1024);
	
}

say int(vsize($$)/1024);


undef $sv;

__END__
keys(%hash) = 2**20;
say int(vsize($$)/1024);
keys(%hash) = 2**21;
say int(vsize($$)/1024);
undef %hash;
say int(vsize($$)/1024);

