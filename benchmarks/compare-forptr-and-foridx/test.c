#include "benchmark.h"
//#include "asmlib.h"
#include <stdint.h>

int test_for (va_list ap) {
	char *buf  = va_arg( ap, char * );
	int   size = va_arg( ap, int );
	char *p;
	int   res = 0;
	for ( p=buf; p - buf < size; p+=2  ) {
		res += *(uint16_t*) p;
	}
	return res;
}

int test_idx (va_list ap) {
	char *buf  = va_arg( ap, char * );
	int   size = va_arg( ap, int ) / 2;
	int   res = 0;
	int   i;
	for ( i=0; i < size; i++  ) {
		res += ((uint16_t*) buf)[i];
	}
	return res;
}

int test_val (va_list ap) {
	char *buf  = va_arg( ap, char * );
	int   size = va_arg( ap, int ) / 2;
	int   res = 0;
	int   i;
	uint16_t val;
	for ( p=buf; p - buf < size; p+=2  ) {
		val = *(uint16_t*) p;
		res += ((uint16_t*) buf)[i];
	}
	return res;
}

static const char *x = "XAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBRXAEDFGBR";
static const char *l = "sdo hfk hlks dhflkjshd lksjhd flkhsd fkkls h kjsdfh l kshd flkjh klsjhkfljh l kl jhsdfkl    khsdkhs lks sdk hfklsh l klshfl ksdd f";
//static const char *s = "klshfl";
static const char *s = "hfklsh l klshfl";
static const char *s2 = "sdo hfk hlks dhflkjshd lksjhd flkhsd fkkls h kjsdfh l kshd flkjh klsjhkfljh l kl jhsdfkl    khsdkhs lks sdk hfklsh l klshfl 1sdd f";
static const char *x1 = "content-length";
static const char *x2 = "Content-length";
static const char *x3 = "transfer-encoding";
static const char *x4 = "transfer1encoding";

int main () {
	bench_item tests1[2] = {
		//{"my",  test_my_strstr,  0},
		{"for", test_for, 0},
		{"idx", test_idx, 0},
		//{"asm", test_asm_strstr, 0}
	};
	benchmark(1E9, sizeof(tests1)/sizeof(bench_item), tests1, x, (int)strlen(x));
	//benchmark(1, sizeof(tests1)/sizeof(bench_item), tests1, "test", 4);
	return 0;
}
