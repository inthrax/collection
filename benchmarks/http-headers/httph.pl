#!/usr/bin/env perl

use strict;
use Data::Dumper;
use Benchmark ':all';
use AnyEvent::Util 'fh_nonblocking';
use HTTP::Easy::Headers;
pipe my $r, my $w or die "$!";
#print {$w} "Test\n" or die "$!";
#print <$r>;

my $buf =
"GET /123213 HTTP/1.1
Host: localhost:8080
User-Agent: weighttp/0.111
Connection: close
Connection: close
more: som
	e

";
#print $buf;
fh_nonblocking $r,1;
#fh_nonblocking $w,1;

my $self = bless{},'main';my $id;
sub drop {
	warn "@_ at @{[ (caller)[1,2] ]}";
}

my %tests = (
	easy => sub {
						if (( my $i = index($buf,"\012", 0) ) > -1) {
							HTTP::Easy::Headers->decode(substr($buf,$i+1));
						}
	},
	re1 => sub {
						if (( my $i = index($buf,"\012", 0) ) > -1) {
							my $ix;
							my $full;
							my $lastkey;
							my %h;
							while (( $i = index($buf,"\012",$ix = $i+1) ) > -1) {
								if ($i - $ix < 2) { $full = 1; last; }
								pos($buf) = $ix;
								if (substr( $buf, $ix, 1 ) =~ /[\011\040]/) { # continuation
									$buf =~ /\G[\011\040]+([^\012]+?)\015?\012/sxogc or next;
									$h{ $lastkey } .= ' '.$1;
								}
								else{
									$buf =~ /\G ([^:\000-\037]+)[\011\040]*:[\011\040]*/sxogc or return $self->drop($id, "Bad header line: ".substr($buf, $ix, $i-$ix));
									$lastkey = lc $1;
									$buf =~ /\G ([^\012]+?)\015?\012/sxogc or return $self->drop($id);
									$h{ $lastkey } = exists $h{ $lastkey } ? $h{ $lastkey }.','.$1 : $1;
								}
							}
							return \%h;
						}
	},
	re3 => sub {
						if (( my $i = index($buf,"\012", 0) ) > -1) {
							local $_ = $buf;
							pos() = $i+1;
							my $ix;
							my $full;
							my $lastkey;
							my %h;
							#while ( index( $_,"\012",pos() ) > -1 ) {
							while(){
								if (/\G[\011\040]+/sxogc) { # continuation
									/\G([^\015\012]+)\015?\012/sxogc or next;
									$h{ $lastkey } .= ' '.$1;
								}
								elsif( /\G ([^:\000-\037]+)[\011\040]*:[\011\040]* ([^\012]+?)\015?\012/sxogc ){
									$lastkey = lc $1;
									$h{ $lastkey } = exists $h{ $lastkey } ? $h{ $lastkey }.','.$2: $2;
								}
								elsif (/\G\015?\012/sxogc) {
									$full = 1;
									last;
								}
								elsif(/\G [^\012]* \Z/sxogc) {
									#warn "Short read XX";
									last; # Partial read
								}
								else {
									warn "Drop";
									$self->drop($id, "Bad header line: ".substr($_, pos(), 10)); # TBD
									last;
								}
							}
							#if ($full) {
							#} else {
							#	warn "short read";
							#}
							return \%h;
						}
	},
	re4 => sub {
						if (( my $i = index($buf,"\012", 0) ) > -1) {
							my $ix;
							my $full;
							my $lastkey;
							my %h;
							pos($buf) = $i+1;
							while(){
								if( $buf =~ /\G ([^:\000-\037\040]+)[\011\040]*:[\011\040]* ([^\012]+?)\015?\012/sxogc ){
									$lastkey = lc $1;
									$h{ $lastkey } = exists $h{ $lastkey } ? $h{ $lastkey }.','.$2: $2;
								}
								elsif ($buf =~ /\G[\011\040]+/sxogc) { # continuation
									$buf =~ /\G([^\015\012]+)\015?\012/sxogc or next;
									$h{ $lastkey } .= ' '.$1;
								}
								elsif ($buf =~ /\G\015?\012/sxogc) {
									$full = 1;
									last;
								}
								elsif($buf =~ /\G [^\012]* \Z/sxogc) {
									last; # Partial read
								}
								else {
									warn "Drop";
									$self->drop($id, "Bad header line: ".substr($buf, pos($buf), 10)); # TBD
									last;
								}
							}
							#if ($full) {
							#} else {
							#	warn "short read";
							#}
							return \%h;
						}
	},
);

for (keys %tests) {
	print $_;
	print Dumper( $tests{$_}() );
}
exit unless $ARGV[0];
cmpthese timethese -$ARGV[0], \%tests;
__END__
	re2 => sub {
						if (( my $i = index($buf,"\012", 0) ) > -1) {
							local $_ = $buf;
							pos() = $i+1;
							my $ix;
							my $full;
							my $lastkey;
							my %h;
							while ( index( $_,"\012",pos() ) > -1 ) {
								if (/\G[\011\040]+/sxogc) { # continuation
									/\G([^\015\012]+)\015?\012/sxogc or next;
									$h{ $lastkey } .= ' '.$1;
								}
								elsif( /\G ([^:\000-\037]+)[\011\040]*:[\011\040]*/sxogc ){
									$lastkey = lc $1;
									/\G ([^\012]+?)\015?\012/sxogc or return $self->drop($id);
									$h{ $lastkey } = exists $h{ $lastkey } ? $h{ $lastkey }.','.$1 : $1;
								}
								elsif (/\G\015?\012/) {
									$full = 1;
									last;
								}
								elsif(/\G [^\012]* \Z/sxogc) {
									last; # Partial read
								}
								else {
									warn "Drop";
									$self->drop($id, "Bad header line: ".substr($_, pos(), 10)); # TBD
									last;
								}
							}
							return \%h;
						}
	},
