use 5.010;
use Benchmark ':all';

=for rem
sub readablePhone {

 my $phone = shift;

 $phone =~ s/\D//g;

 $phone =~ s/^(.)(9..|499|495|812)(...)(..)(..)$/$1 ($2) $3-$4-$5/g;

 return "+$phone";

}
=cut

sub hiddenPhone {

 my $phone = shift;

 my $cnt = 0;

 my @chars = split //, $phone;

 @chars = reverse map {

   my $char = $_;

   if($cnt < 4 && $char =~ /^\d$/) {

    $char = "*";

    $cnt++;

   };

   $char;

  } reverse @chars;

 $phone = join "", @chars;

 return $phone;

}

my $phone = "+7(926)230-98-05";
say hiddenPhone($phone);
#$phone =~ s{\d(\D*)\d(\D*)\d(\D*)\d(\D*)$}{*$1*$2*$3*$4};
#say $phone;
#exit;

cmpthese timethese -1,{
	custom  => sub { hiddenPhone($phone) },
	regexp  => sub { my $p = $phone; $p =~ s{\d(\D*)\d(\D*)\d(\D*)\d(\D*)$}{*$1*$2*$3*$4}; },
	regexp2  => sub { my $p = reverse $phone; $p =~ s{^(\D*)\d(\D*)\d(\D*)\d(\D*)\d}{$1*$2*$3*$4*};$p = reverse $p },
};
