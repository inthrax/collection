#!/usr/bin/env perl

use Benchmark ':all';

cmpthese timethese -1, {
	'replace10' => sub {
		my $a = "x"x1000;
		$a = substr($a,990);
	},
	'substr10' => sub {
		my $a = "x"x1000;
		substr($a,0,990,'');
	},
};

cmpthese timethese -1, {
	'replace500' => sub {
		my $a = "x"x1000;
		$a = substr($a,500);
	},
	'substr500' => sub {
		my $a = "x"x1000;
		substr($a,0,500,'');
	},
};

cmpthese timethese -1, {
	'replace990' => sub {
		my $a = "x"x1000;
		$a = substr($a,10);
	},
	'substr990' => sub {
		my $a = "x"x1000;
		substr($a,0,10,'');
	},
};
