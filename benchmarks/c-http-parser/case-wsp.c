#include "benchmark.h"

#ifndef likely
#define likely(x)       __builtin_expect(!!(x), 1)
#define unlikely(x)     __builtin_expect(!!(x), 0)
#endif

#define case_wsp   \
		case 0x20 :\
		case 0xa  :\
		case 0xd  :\
		case 0x9

static char wsp[256] = {
	0,0,0,0,0,0,0,0,0,1,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
};

int test_default (va_list ap) {
	char *arg = va_arg( ap, char * );
	register char *p = arg;
	while (1) {
		if (unlikely(*p == 0)) return p - arg;
		switch(*p) {
			case_wsp: p++; break;
			default: break;
		}
	}
}
int test_optimized (va_list ap) {
	char *arg = va_arg( ap, char * );
	register char *p = arg;
	while (1) {
		if (unlikely(*p == 0)) return p - arg;
		if (likely(wsp[*p])) {
			p++;
		} else {
			p++;
		}
	}
}

static const char *l = "             \n\n\n\n\n\n\n\t\t\t\r\r                    ";

int main () {
	bench_item tests1[2] = {
		//{"my",  test_my_strstr,  0},
		{"def", test_default, 0},
		{"opt", test_optimized, 0}
	};
	benchmark(1E9, sizeof(tests1)/sizeof(bench_item), tests1, l);
	return 0;
}
