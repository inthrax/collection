#include "benchmark.h"
#include "asmlib.h"

int test_my_strstr (va_list ap) {
	char *str1 = va_arg( ap, char * );
	char *str2 = va_arg( ap, char * );
	char *b = 0;
	char *p = str1;
	char *x = str2;
	while (*p) {
		if (*p == *x) {
			if (!b) b = p;
			p++;x++;
			if (!*x) break;
		} else {
			b = 0;
			x = str2;
			p++;
		}
	}
	return (int)(str1 - b);
}
int test_std_strstr (va_list ap) {
	char *str1 = va_arg( ap, char * );
	char *str2 = va_arg( ap, char * );
	return (int)( str1 - strstr(str1,str2));
}
int test_asm_strstr (va_list ap) {
	char *str1 = va_arg( ap, char * );
	char *str2 = va_arg( ap, char * );
	return (int)( str1 - A_strstr(str1,str2));
}

int test_std_strcmp (va_list ap) {
	char *str1 = va_arg( ap, char * );
	char *str2 = va_arg( ap, char * );
	return strcasecmp(str1,str2);
}
int test_asm_strcmp (va_list ap) {
	char *str1 = va_arg( ap, char * );
	char *str2 = va_arg( ap, char * );
	return A_stricmp(str1,str2);
}

static const char *l = "sdo hfk hlks dhflkjshd lksjhd flkhsd fkkls h kjsdfh l kshd flkjh klsjhkfljh l kl jhsdfkl    khsdkhs lks sdk hfklsh l klshfl ksdd f";
//static const char *s = "klshfl";
static const char *s = "hfklsh l klshfl";
static const char *s2 = "sdo hfk hlks dhflkjshd lksjhd flkhsd fkkls h kjsdfh l kshd flkjh klsjhkfljh l kl jhsdfkl    khsdkhs lks sdk hfklsh l klshfl 1sdd f";
static const char *x1 = "content-length";
static const char *x2 = "Content-length";
static const char *x3 = "transfer-encoding";
static const char *x4 = "transfer1encoding";

int main () {
	bench_item tests1[2] = {
		//{"my",  test_my_strstr,  0},
		{"std", test_std_strstr, 0},
		{"asm", test_asm_strstr, 0}
	};
	benchmark(1E9, sizeof(tests1)/sizeof(bench_item), tests1, l, s);
	return 0;
	bench_item tests2[2] = {
		{"std", test_std_strcmp, 0},
		{"asm", test_asm_strcmp, 0}
	};
	benchmark(1E9, 2, tests2, l, s2);
	benchmark(1E9, 2, tests2, x1, x2);
	benchmark(1E9, 2, tests2, x1, x3);
	benchmark(1E9, 2, tests2, x4, x3);
}
