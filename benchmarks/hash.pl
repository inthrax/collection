use Benchmark ':all';

%low = map { +($_ => $_) } 1..100;
%big = map { +( $_ => $_ ) } 1..10_0000;
cmpthese
timethese -1, {
	big => sub {  return $big{ int(rand(10_0000)) } },
	low => sub {  return $low{ int(rand(100)) }  }
};