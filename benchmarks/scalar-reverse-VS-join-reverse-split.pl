#!/usr/bin/env perl

use Benchmark ':all';

my $str = "x"x32;

sub str_reverse($) {
	return join '', reverse split //, shift;
}

cmpthese timethese -1, {
	'native' => sub { my $new = reverse $str; },
	'xxxxxx' => sub { my $new = str_reverse $str; },
};