BEGIN {
	$INC{'EV.pm'} = 1;
	our $loop;
	*EV::import = sub { require AnyEvent; require AnyEvent::Impl::Perl; };
	*EV::loop = sub (;$) {
		$loop and die "Nested loop at @{[ (caller)[1,2] ]}";
		$loop = AnyEvent->condvar;
		$loop->recv;
	};
	*EV::unloop = sub (;$) {
		$loop or die "Loop not running at @{[ (caller)[1,2] ]}";
		$loop->send;
		undef $loop;
	};
	our $AUTOLOAD;
	*EV::AUTOLOAD = sub {
		warn "used $AUTOLOAD at @{[ (caller)[1,2] ]}\n";
	};
}
1;
