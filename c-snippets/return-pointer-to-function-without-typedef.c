#include <stdio.h>

typedef int (*ptr) (char *, size_t, const char *, ...);
ptr function () { return snprintf; }

int (*f1 ()) (char *, size_t, const char *, ...) {
	return snprintf;
}

int main () {
	char buf[256];
	f1()( buf,255,"%s", "OK" );
	printf("%s\n",buf);
}