{
	package My::Graphite;
	
	sub new {
		my $pkg = shift;
		my %args = @_;
		if ($args{type} eq 'udp') {
			return My::Graphite::UDP->new(@_);
		} else {
			return My::Graphite::TCP->new(@_);
		}
	}
	
	package My::Graphite::UDP;
	
	package My::Graphite::TCP;
	
	use strict;
	use AnyEvent::Socket;
	use Carp;
	use Scalar::Util 'weaken';
	use uni::perl ':dumper';
	
	sub udp_connect($$$;$);
	
	sub new {
		my $pkg = shift;
		my $self = bless {
			host => 0,
			port => 2003,
			mem_limit => 1024,
			@_,
		}, $pkg;
		# TODO
		#$self->{failover} = {
		#	size => 100*1024*1024, # 100Mb
		#	path => "/tmp/graphite.$self->{host}.$self->{port}",
		#} unless exists $self->{failover};
		#if (exists $self->{failover}{path}) {
		#	if( open $self->{failover}{fh}, "+<", $self->{failover}{path} ) {
		#		select((select($self->{failover}{fh}), $|++ )[0]);
		#		if (-s $self->{failover}{fh}) {
		#		}
		#	} else {
		#		warn "Cant open failover `$self->{failover}{path}': $!";
		#	}
		#}
		$self;
	}
	
	sub push_write {
		my $self = shift;
		ref $_[0] and die "Writing refs not supported yet";
		$self->{wsize}++;
		$self->{seq}++;
		my $l = { w => $_[0], s => $self->{seq} };
		$self->{wlast}{next} = $l;
		$self->{wlast} = $l;
		#warn dumper $self->{wbuf};
		$self->_ww;
	}
	
	sub _disconnected {
		my $self = shift;
		warn "Connection reset!: @_";
		$self->connect;
	}
	sub _wio {
		my $self = shift;
				weaken($self);
				my $w;
				#warn "$self->{fh}";
				$self->{ww} = AE::io $self->{fh}, 1, my $cb = sub {
					$self or return;
					while ( $self->{wsize} > 0 ) {
						#warn "rw.io [$self->{wsize}]  ";
						#warn dumper $self;
						
						if (!length $self->{wbuf}{w}) {
							if ( $self->{wbuf}{next} ) {
								#warn "take next: $self->{wbuf}{next}{w} ";
								$self->{wbuf} = $self->{wbuf}{next};
								$self->{wsize}--;
								next;
							} else {
								#warn "no more";
								delete $self->{ww};
								$self->{wbuf} = $self->{wlast} = {};
								return;
							}
						}
						#warn "sending $self->{wbuf}{w} ";
						$w = syswrite( $self->{fh}, $self->{wbuf}{w} );
						
						if ( $w == length $self->{wbuf}{w} ) {
							#warn "[$self->{wsize}] sent $self->{wbuf}{w} ";
							if ( $self->{wbuf}{next} ) {
								$self->{wbuf} = $self->{wbuf}{next};
								$self->{wsize}--;
							} else {
								delete $self->{ww};
								$self->{wsize} = 0;
								delete $self->{wbuf};
								delete $self->{wlast};
								return;
							}
						}
						elsif (defined $w) {
							$self->{wbuf}{w} = substr( $self->{wbuf}{w}, $w );
							return;
						}
						else {
							delete $self->{fh};
							delete $self->{ww};
							
							
							return $self->_disconnected("$!");
						}
					}
				};
		return $cb;
	}
	
	sub send:method {
		my ($self, $id, $value, $time) = @_;
		return carp "Need id and value" unless $id and defined $value;
		$time ||= int(time);
		my $buf = sprintf "%s %f %d\n", $id, $value, $time;
		#warn "send called $buf  ";
		if ( $self->{wsize} ) {
			#warn "pushed_to queue because of $self->{wsize}";
			$self->{wsize}++;
			my $l = { w => $buf };
			$self->{wlast}{next} = $l;
			$self->{wlast}       = $l;
			while ($self->{mem_limit} and $self->{wsize} > $self->{mem_limit}) {
				$self->{wbuf} = $self->{wbuf}{next};
				$self->{wsize}--;
			}
			#$self->_failover($buf) unless $self->{fh};
			$self->connect unless $self->{cnn};
			return;
		}
		if ($self->{fh}) {
			
			my $w = syswrite( $self->{fh}, $buf );
			if ($w == length $buf) {
				#warn "sent $buf ";
				# ok
			}
			elsif (defined $w) {
				substr($buf,0,$w,'');
				$self->{wsize} = 1;
				$self->{wbuf} = $self->{wlast} = { w => $buf };
			}
			else {
				$self->{wsize} = 1;
				$self->{wbuf} = $self->{wlast} = {};
				return $self->_disconnected("$!");
			}
			return;
			
		} else {
			#warn "not connected for $buf";
			$self->{wsize} = 1;
			$self->{wbuf} = $self->{wlast} = { w => $buf };
			$self->connect;
			
		}
	}
	
	sub _failover {
		my $self = shift;
		my $buf = shift;
		return unless $self->{failover};
		if (exists $self->{failover}{path}) {
			print { $self->{failover}{fh} } $buf
				or warn "Can't write to failover: $!";
		}
	}
	
	sub connect {
		my $self = shift;
		#warn "connection";
		if ($self->{type} eq 'udp') {
			$self->{cnn} = udp_connect $self->{host},$self->{port}, sub {
				$self or return;
				if (my $fh = shift) {
					warn "connected: @_";
					$self->{fh} = $fh;
					$self->_wio()->();
				} else {
					warn "connect failed: $!";
					$self->{cnn} = AE::timer 1,0,sub {
						$self or return;
						$self->connect;
					};
				}
			};
		} else {
			$self->{cnn} = tcp_connect $self->{host},$self->{port}, sub {
				$self or return;
				if (my $fh = shift) {
					warn "connected: @_";
					$self->{fh} = $fh;
					$self->_wio()->();
				} else {
					warn "connect failed: $!";
					$self->{cnn} = AE::timer 1,0,sub {
						$self or return;
						$self->connect;
					};
				}
			};
		}
	}
	
	sub stop {
		
	}


sub udp_connect($$$;$) {
	my ($host, $port, $connect, $prepare) = @_;
	my %state = ( fh => undef );
	# name/service to type/sockaddr resolution
	AnyEvent::Socket::resolve_sockaddr $host, $port, "udp", 0, SOCK_DGRAM, sub {
		my @target = @_;
		$state{next} = sub {
			return unless exists $state{fh};
			my $errno = $!;
			my $target = shift @target
				or return AE::postpone {
					return unless exists $state{fh};
					%state = ();
					$! = $errno;
					$connect->();
			};
			my ($domain, $type, $proto, $sockaddr) = @$target;
			socket $state{fh}, $domain, $type, $proto
				or return $state{next}();
			fh_nonblocking $state{fh}, 1;
			my $timeout = $prepare && $prepare->($state{fh});
			$timeout ||= 30 if AnyEvent::WIN32;
			$state{to} = AE::timer $timeout, 0, sub {
				$! = Errno::ETIMEDOUT;
				$state{next}();
			} if $timeout;
			if ( (connect $state{fh}, $sockaddr)
				|| ($! == Errno::EINPROGRESS # POSIX
				|| $! == Errno::EWOULDBLOCK
				|| $! == AnyEvent::Util::WSAEINVAL # not convinced, but doesn't hurt
				|| $! == AnyEvent::Util::WSAEWOULDBLOCK)
			) {
				$state{ww} = AE::io $state{fh}, 1, sub {
					if (my $sin = getpeername $state{fh}) {
						my ($port, $host) = AnyEvent::Socket::unpack_sockaddr $sin;
						delete $state{ww}; delete $state{to};
						my $guard = guard { %state = () };
						$connect->(delete $state{fh}, AnyEvent::Socket::format_address $host, $port, sub {
							$guard->cancel;
							$state{next}();
						});
					} else {
						if ($! == Errno::ENOTCONN) {
							# maybe recv?
							sysread $state{fh}, my $buf, 1;
							$! = (unpack "l", getsockopt $state{fh}, Socket::SOL_SOCKET(), Socket::SO_ERROR()) || Errno::EAGAIN
								if AnyEvent::CYGWIN && $! == Errno::EAGAIN;
						}
						return if $! == Errno::EAGAIN; # skip spurious wake-ups
						delete $state{ww}; delete $state{to};
						$state{next}();
					}
				};
			} else {
				$state{next}();
			}
		};
		$! = Errno::ENXIO;
		$state{next}();
	};
	defined wantarray && guard { %state = () };
}
}

package main;

use EV;
use uni::perl ':dumper';
use AnyEvent::HTTP;
use Time::HiRes 'time';
use JSON::XS;
my $JSON = JSON::XS->new()->utf8->pretty;


my $g = My::Graphite->new( type => 'udp' );
my $t; $t = AE::timer 0.1,1, sub {
	$g,$t;
	http_request
		GET => 'http://www.winston.ru/static/wsf/prizes.json?_='.int(time * 1000),
		sub => sub {
		eval {
			my$json = $JSON->decode( $_[0] );
			#warn $JSON->encode($json);
			for my $p (@{ $json->{prize} }) {
				$g->send("winston.".$p->{prizeSystemName}, $p->{countLeft});
				#print "$p->{prizeSystemName}: $p->{countLeft}\n";
			}
		}
		};
		
};


EV::loop;

__END__

		my $write = sub {
			$self and exists $self->{$id} or return;
			for my $buf (@_) {
				ref $buf or do { $buf = \( my $str = $buf ); warn "Passed nonreference buffer from @{[ (caller)[1,2] ]}\n"; };
				#warn "Write $$buf";
				if ( $self->{$id}{wbuf} ) {
					$self->{$id}{closeme} and return warn "Write ($$buf) called while connection close was enqueued at @{[ (caller)[1,2] ]}";
					${ $self->{$id}{wbuf} } .= defined $$buf ? $$buf : return $self->{$id}{closeme} = 1;
					return;
				}
				elsif ( !defined $$buf ) { return $drop->($id); }
				
				my $w = syswrite( $self->{$id}{fh}, $$buf );
				if ($w == length $$buf) {
					# ok;
				}
				elsif (defined $w) {
					substr($$buf,0,$w,'');
					$self->{$id}{wbuf} = $buf;
					$self->{$id}{ww} = AE::io $self->{$id}{fh}, 1, sub {
						#warn "ww.io.$id";
						$self and exists $self->{$id} or return;
						$w = syswrite( $self->{$id}{fh}, ${ $self->{$id}{wbuf} } );
						if ($w == length ${ $self->{$id}{wbuf} }) {
							delete $self->{$id}{wbuf};
							delete $self->{$id}{ww};
							if( $self->{$id}{closeme} ) { $drop->($id); }
						}
						elsif (defined $w) {
							${ $self->{$id}{wbuf} } = substr( ${ $self->{$id}{wbuf} }, $w );
							#substr( ${ $self->{$id}{wbuf} }, 0, $w, '');
						}
						else { return $drop->($id, "$!"); }
					};
				}
				else { return $drop->($id, "$!"); }
			}
		};
