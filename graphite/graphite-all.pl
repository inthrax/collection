{
	package My::Graphite;

	use strict;
	use Carp;
	use Scalar::Util 'weaken';
	
	use AnyEvent::Socket;
	use AnyEvent::Util qw(fh_nonblocking AF_INET6 guard);
	use Socket qw(AF_INET AF_UNIX SOCK_STREAM SOCK_DGRAM SOL_SOCKET SO_REUSEADDR);
	use Errno ();
	
	use uni::perl ':dumper';
	
	sub new {
		my $pkg = shift;
		my $self = bless {
			host => 0,
			port => 2003,
			mem_limit => 1024,
			type => "tcp",
			timeout => 5,
			@_,
		}, $pkg;
		$self;
	}
	
	
	sub _connect {
		my $self = shift;
		my $connect = pop;
		my ($host,$port) = ($self->{host},$self->{port});
		my %state = (fh => undef);
		AnyEvent::Socket::resolve_sockaddr
			$self->{host}, $self->{port},
			$self->{type}, 0,
			$self->{type} eq "udp" ? SOCK_DGRAM : SOCK_STREAM,
		sub {
			my @target = @_;
			$state{next} = sub {
				return unless exists $state{fh};
				my $errno = $!;
				my $target = shift @target
					or return AE::postpone {
						return unless exists $state{fh};
						%state = (); $! = $errno; $connect->();
					};
				
				my ($domain, $type, $proto, $sockaddr) = @$target;
				socket $state{fh}, $domain, $type, $proto or return $state{next}();
				
				fh_nonblocking $state{fh}, 1;
				my $timeout = $self->{timeout};$timeout ||= 30 if AnyEvent::WIN32;
				
				$state{to} = AE::timer $timeout, 0, sub { $! = Errno::ETIMEDOUT; $state{next}(); } if $timeout;
				
				if ( (connect $state{fh}, $sockaddr)
					|| ($! == Errno::EINPROGRESS # POSIX
					|| $! == Errno::EWOULDBLOCK
					|| $! == AnyEvent::Util::WSAEINVAL # not convinced, but doesn't hurt
					|| $! == AnyEvent::Util::WSAEWOULDBLOCK)
				) {
					$state{ww} = AE::io $state{fh}, 1, sub {
						if (my $sin = getpeername $state{fh}) {
							my ($port, $host) = AnyEvent::Socket::unpack_sockaddr $sin;
							delete $state{ww}; delete $state{to};
							my $guard = guard { %state = () };
							$connect->(delete $state{fh}, AnyEvent::Socket::format_address $host, $port, sub {
								$guard->cancel;
								$state{next}();
							});
						} else {
							if ($! == Errno::ENOTCONN) {
								# maybe recv?
								sysread $state{fh}, my $buf, 1;
								$! = (unpack "l", getsockopt $state{fh}, Socket::SOL_SOCKET(), Socket::SO_ERROR()) || Errno::EAGAIN
									if AnyEvent::CYGWIN && $! == Errno::EAGAIN;
							}
							return if $! == Errno::EAGAIN; # skip spurious wake-ups
							delete $state{ww}; delete $state{to};
							$state{next}();
						}
					};
				} else {
					$state{next}();
				}
			};
			$! = Errno::ENXIO;
			$state{next}();
		};
		defined wantarray && guard { %state = () };
	}
	
	sub _disconnected {
		my $self = shift;
		warn "$self->{type} connection reset: @_";
		$self->connect;
	}
	
	sub connect {
		my $self = shift;
		$self->{cnn} = $self->_connect( sub {
			$self or return;
			if (my $fh = shift) {
				warn "$self->{type} connected: @_";
				$self->{fh} = $fh;
				$self->_wio()->();
			} else {
				warn "$self->{type} connect failed: $!";
				$self->{cnn} = AE::timer 1,0,sub {
					$self or return;
					$self->connect;
				};
			}
		} );
	}
	
	sub _wio {
		my $self = shift;
				weaken($self);
				my $w;
				#warn "$self->{fh}";
				$self->{ww} = AE::io $self->{fh}, 1, my $cb = sub {
					$self or return;
					while ( $self->{wsize} > 0 ) {
						warn "$self->{type}.rw.io [$self->{wsize}]  ";
						#warn dumper $self;
						
						if (!length $self->{wbuf}{w}) {
							if ( $self->{wbuf}{next} ) {
								#warn "take next: $self->{wbuf}{next}{w} ";
								$self->{wbuf} = $self->{wbuf}{next};
								$self->{wsize}--;
								next;
							} else {
								#warn "no more";
								delete $self->{ww};
								$self->{wbuf} = $self->{wlast} = {};
								return;
							}
						}
						#warn "sending $self->{wbuf}{w} ";
						$w = send( $self->{fh}, $self->{wbuf}{w}, 0 );
						
						if ( defined($w) ) {
							warn sprintf "Partial send: sent %d of %d",$w,length $self->{wbuf}{w} if ($w != length $self->{wbuf}{w});
							warn "$self->{type} sent [$self->{wsize}] $self->{wbuf}{w} ";
							if ( $self->{wbuf}{next} ) {
								$self->{wbuf} = $self->{wbuf}{next};
								$self->{wsize}--;
							} else {
								delete $self->{ww};
								$self->{wsize} = 0;
								delete $self->{wbuf};
								delete $self->{wlast};
								return;
							}
						}
						elsif ( $! == Errno::EAGAIN or $! == Errno::EINTR ) {
							return;
						}
						else {
							warn "[$self->{type}] io send failed [$self->{wsize}]: $!";
							delete $self->{fh};
							delete $self->{ww};
							return $self->_disconnected("$!");
						}
					}
				};
		return $cb;
	}
	
	sub send:method {
		my ($self, $id, $value, $time) = @_;
		return carp "Need id and value" unless $id and defined $value;
		$time ||= int(time);
		my $buf = sprintf "%s %f %d\n", $id, $value, $time;
		#warn "send called $buf  ";
		if ( $self->{wsize} ) {
			#warn "pushed_to queue because of $self->{wsize}";
			$self->{wsize}++;
			my $l = { w => $buf };
			$self->{wlast}{next} = $l;
			$self->{wlast}       = $l;
			while ($self->{mem_limit} and $self->{wsize} > $self->{mem_limit}) {
				$self->{wbuf} = $self->{wbuf}{next};
				$self->{wsize}--;
			}
			#$self->_failover($buf) unless $self->{fh};
			$self->connect unless $self->{cnn};
			return;
		}
		if ($self->{fh}) {
			my $w = send( $self->{fh}, $buf, 0 );
			if (defined $w) {
				warn sprintf "Partial send: sent %d of %d",$w,length $buf if ($w != length $buf);
				#warn "sent $buf ";
				# ok
			}
			else {
				warn "[$self->{type}] first send failed [$self->{wsize}]: $!";
				$self->{wsize} = 1;
				$self->{wbuf} = $self->{wlast} = { w => $buf };
				if ( $! == Errno::EAGAIN or $! == Errno::EINTR ) {
					return $self->_wio();
				} else {
					return $self->_disconnected("$!");
				}
			}
		} else {
			#warn "not connected for $buf";
			$self->{wsize} = 1;
			$self->{wbuf} = $self->{wlast} = { w => $buf };
			$self->connect;
			
		}
	}
}

package main;

use EV;
use uni::perl ':dumper';
use AnyEvent::HTTP;
use Time::HiRes 'time';
use JSON::XS;
my $JSON = JSON::XS->new()->utf8->pretty;



my $udp = My::Graphite->new( type => 'udp' );
my $tcp = My::Graphite->new( type => 'tcp' );
my $carb = My::Graphite->new( type => 'udp', port => 2005 );
my $carb2 = My::Graphite->new( type => 'udp', port => 2006 );
my $eth = My::Graphite->new( type => 'udp', port => 2004 );
my $t; $t = AE::timer 0.1,1, sub {
	$t;
	$tcp->send("tcp", time - 1);
	$udp->send("udp", time);
	$carb->send("test1", rand(100));
	$carb2->send("test", rand(100));
	$eth->send("test",rand(100));
	return;
};


EV::loop;

__END__

		my $write = sub {
			$self and exists $self->{$id} or return;
			for my $buf (@_) {
				ref $buf or do { $buf = \( my $str = $buf ); warn "Passed nonreference buffer from @{[ (caller)[1,2] ]}\n"; };
				#warn "Write $$buf";
				if ( $self->{$id}{wbuf} ) {
					$self->{$id}{closeme} and return warn "Write ($$buf) called while connection close was enqueued at @{[ (caller)[1,2] ]}";
					${ $self->{$id}{wbuf} } .= defined $$buf ? $$buf : return $self->{$id}{closeme} = 1;
					return;
				}
				elsif ( !defined $$buf ) { return $drop->($id); }
				
				my $w = syswrite( $self->{$id}{fh}, $$buf );
				if ($w == length $$buf) {
					# ok;
				}
				elsif (defined $w) {
					substr($$buf,0,$w,'');
					$self->{$id}{wbuf} = $buf;
					$self->{$id}{ww} = AE::io $self->{$id}{fh}, 1, sub {
						#warn "ww.io.$id";
						$self and exists $self->{$id} or return;
						$w = syswrite( $self->{$id}{fh}, ${ $self->{$id}{wbuf} } );
						if ($w == length ${ $self->{$id}{wbuf} }) {
							delete $self->{$id}{wbuf};
							delete $self->{$id}{ww};
							if( $self->{$id}{closeme} ) { $drop->($id); }
						}
						elsif (defined $w) {
							${ $self->{$id}{wbuf} } = substr( ${ $self->{$id}{wbuf} }, $w );
							#substr( ${ $self->{$id}{wbuf} }, 0, $w, '');
						}
						else { return $drop->($id, "$!"); }
					};
				}
				else { return $drop->($id, "$!"); }
			}
		};
