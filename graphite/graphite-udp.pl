{
	package My::Graphite;
	
	sub new {
		my $pkg = shift;
		my %args = @_;
		if ($args{type} eq 'udp') {
			return My::Graphite::UDP->new(@_);
		} else {
			return My::Graphite::TCP->new(@_);
		}
	}
	
	sub _disconnected {
		my $self = shift;
		warn "Connection reset!: @_";
		$self->connect;
	}
	
	sub connect {
		
	}
	
	package My::Graphite::UDP;
	
	use strict;
	use AnyEvent::Socket;
	use Carp;
	use Scalar::Util 'weaken';
	use AnyEvent::Util qw(fh_nonblocking AF_INET6 guard);
	use Errno ();
	use Socket qw(AF_INET AF_UNIX SOCK_STREAM SOCK_DGRAM SOL_SOCKET SO_REUSEADDR);
	BEGIN { our @ISA = "My::Graphite" }
	
	use uni::perl ':dumper';
	
sub udp_connect($$$;$) {
	my ($host, $port, $connect, $prepare) = @_;
	my %state = ( fh => undef );
	# name/service to type/sockaddr resolution
	AnyEvent::Socket::resolve_sockaddr $host, $port, "udp", 0, SOCK_DGRAM, sub {
		my @target = @_;
		$state{next} = sub {
			return unless exists $state{fh};
			my $errno = $!;
			my $target = shift @target
				or return AE::postpone {
					return unless exists $state{fh};
					%state = ();
					$! = $errno;
					$connect->();
			};
			my ($domain, $type, $proto, $sockaddr) = @$target;
			socket $state{fh}, $domain, $type, $proto
				or return $state{next}();
			fh_nonblocking $state{fh}, 1;
			my $timeout = $prepare && $prepare->($state{fh});
			$timeout ||= 30 if AnyEvent::WIN32;
			$state{to} = AE::timer $timeout, 0, sub {
				$! = Errno::ETIMEDOUT;
				$state{next}();
			} if $timeout;
			if ( (connect $state{fh}, $sockaddr)
				|| ($! == Errno::EINPROGRESS # POSIX
				|| $! == Errno::EWOULDBLOCK
				|| $! == AnyEvent::Util::WSAEINVAL # not convinced, but doesn't hurt
				|| $! == AnyEvent::Util::WSAEWOULDBLOCK)
			) {
				$state{ww} = AE::io $state{fh}, 1, sub {
					if (my $sin = getpeername $state{fh}) {
						my ($port, $host) = AnyEvent::Socket::unpack_sockaddr $sin;
						delete $state{ww}; delete $state{to};
						my $guard = guard { %state = () };
						$connect->(delete $state{fh}, AnyEvent::Socket::format_address $host, $port, sub {
							$guard->cancel;
							$state{next}();
						});
					} else {
						if ($! == Errno::ENOTCONN) {
							# maybe recv?
							sysread $state{fh}, my $buf, 1;
							$! = (unpack "l", getsockopt $state{fh}, Socket::SOL_SOCKET(), Socket::SO_ERROR()) || Errno::EAGAIN
								if AnyEvent::CYGWIN && $! == Errno::EAGAIN;
						}
						return if $! == Errno::EAGAIN; # skip spurious wake-ups
						delete $state{ww}; delete $state{to};
						$state{next}();
					}
				};
			} else {
				$state{next}();
			}
		};
		$! = Errno::ENXIO;
		$state{next}();
	};
	defined wantarray && guard { %state = () };
}
	
	sub new {
		my $pkg = shift;
		my $self = bless {
			host => 0,
			port => 2003,
			mem_limit => 1024,
			connfunc => \&udp_connect,
			sendfunc => defined &CORE::GLOBAL::send ? \&CORE::GLOBAL::send : \&CORE::send,
			@_,
		}, $pkg;
		$self;
	}
	
	sub _wio {
		my $self = shift;
				weaken($self);
				my $w;
				#warn "$self->{fh}";
				$self->{ww} = AE::io $self->{fh}, 1, my $cb = sub {
					$self or return;
					while ( $self->{wsize} > 0 ) {
						#warn "rw.io [$self->{wsize}]  ";
						#warn dumper $self;
						
						if (!length $self->{wbuf}{w}) {
							if ( $self->{wbuf}{next} ) {
								#warn "take next: $self->{wbuf}{next}{w} ";
								$self->{wbuf} = $self->{wbuf}{next};
								$self->{wsize}--;
								next;
							} else {
								#warn "no more";
								delete $self->{ww};
								$self->{wbuf} = $self->{wlast} = {};
								return;
							}
						}
						#warn "sending $self->{wbuf}{w} ";
						$w = send( $self->{fh}, $self->{wbuf}{w}, 0 );
						
						if ( defined($w) ) {
							warn sprintf "Partial send: sent %d of %d",$w,length $self->{wbuf}{w} if ($w != length $self->{wbuf}{w});
							#warn "[$self->{wsize}] sent $self->{wbuf}{w} ";
							if ( $self->{wbuf}{next} ) {
								$self->{wbuf} = $self->{wbuf}{next};
								$self->{wsize}--;
							} else {
								delete $self->{ww};
								$self->{wsize} = 0;
								delete $self->{wbuf};
								delete $self->{wlast};
								return;
							}
						}
						elsif ( $! == Errno::EAGAIN or $! == Errno::EINTR ) {
							return;
						}
						else {
							delete $self->{fh};
							delete $self->{ww};
							return $self->_disconnected("$!");
						}
					}
				};
		return $cb;
	}
	
	sub send:method {
		my ($self, $id, $value, $time) = @_;
		return carp "Need id and value" unless $id and defined $value;
		$time ||= int(time);
		my $buf = sprintf "%s %f %d\n", $id, $value, $time;
		#warn "send called $buf  ";
		if ( $self->{wsize} ) {
			#warn "pushed_to queue because of $self->{wsize}";
			$self->{wsize}++;
			my $l = { w => $buf };
			$self->{wlast}{next} = $l;
			$self->{wlast}       = $l;
			while ($self->{mem_limit} and $self->{wsize} > $self->{mem_limit}) {
				$self->{wbuf} = $self->{wbuf}{next};
				$self->{wsize}--;
			}
			#$self->_failover($buf) unless $self->{fh};
			$self->connect unless $self->{cnn};
			return;
		}
		if ($self->{fh}) {
			my $w = send( $self->{fh}, $buf, 0 );
			if (defined $w) {
				warn sprintf "Partial send: sent %d of %d",$w,length $buf if ($w != length $buf);
				#warn "sent $buf ";
				# ok
			}
			else {
				$self->{wsize} = 1;
				$self->{wbuf} = $self->{wlast} = { w => $buf };
				if ( $! == Errno::EAGAIN or $! == Errno::EINTR ) {
					return $self->_wio();
				} else {
					return $self->_disconnected("$!");
				}
			}
		} else {
			#warn "not connected for $buf";
			$self->{wsize} = 1;
			$self->{wbuf} = $self->{wlast} = { w => $buf };
			$self->connect;
			
		}
	}
	
	sub connect {
		my $self = shift;
			$self->{cnn} = $self->{connfunc}( $self->{host},$self->{port}, sub {
				$self or return;
				if (my $fh = shift) {
					warn "UDP connected: @_";
					$self->{fh} = $fh;
					$self->_wio()->();
				} else {
					warn "UDP connect failed: $!";
					$self->{cnn} = AE::timer 1,0,sub {
						$self or return;
						$self->connect;
					};
				}
			} );
	}
	
	package My::Graphite::TCP;
	
	use strict;
	use AnyEvent::Socket;
	use Carp;
	use Scalar::Util 'weaken';
	use uni::perl ':dumper';
	
	sub udp_connect($$$;$);
	
	sub new {
		my $pkg = shift;
		my $self = bless {
			host => 0,
			port => 2003,
			mem_limit => 1024,
			@_,
		}, $pkg;
		# TODO
		#$self->{failover} = {
		#	size => 100*1024*1024, # 100Mb
		#	path => "/tmp/graphite.$self->{host}.$self->{port}",
		#} unless exists $self->{failover};
		#if (exists $self->{failover}{path}) {
		#	if( open $self->{failover}{fh}, "+<", $self->{failover}{path} ) {
		#		select((select($self->{failover}{fh}), $|++ )[0]);
		#		if (-s $self->{failover}{fh}) {
		#		}
		#	} else {
		#		warn "Cant open failover `$self->{failover}{path}': $!";
		#	}
		#}
		$self;
	}
	
	sub _wio {
		my $self = shift;
				weaken($self);
				my $w;
				#warn "$self->{fh}";
				$self->{ww} = AE::io $self->{fh}, 1, my $cb = sub {
					$self or return;
					while ( $self->{wsize} > 0 ) {
						#warn "rw.io [$self->{wsize}]  ";
						#warn dumper $self;
						
						if (!length $self->{wbuf}{w}) {
							if ( $self->{wbuf}{next} ) {
								#warn "take next: $self->{wbuf}{next}{w} ";
								$self->{wbuf} = $self->{wbuf}{next};
								$self->{wsize}--;
								next;
							} else {
								#warn "no more";
								delete $self->{ww};
								$self->{wbuf} = $self->{wlast} = {};
								return;
							}
						}
						#warn "sending $self->{wbuf}{w} ";
						$w = syswrite( $self->{fh}, $self->{wbuf}{w} );
						
						if ( $w == length $self->{wbuf}{w} ) {
							#warn "[$self->{wsize}] sent $self->{wbuf}{w} ";
							if ( $self->{wbuf}{next} ) {
								$self->{wbuf} = $self->{wbuf}{next};
								$self->{wsize}--;
							} else {
								delete $self->{ww};
								$self->{wsize} = 0;
								delete $self->{wbuf};
								delete $self->{wlast};
								return;
							}
						}
						elsif (defined $w) {
							$self->{wbuf}{w} = substr( $self->{wbuf}{w}, $w );
							return;
						}
						else {
							delete $self->{fh};
							delete $self->{ww};
							
							
							return $self->_disconnected("$!");
						}
					}
				};
		return $cb;
	}
	
	sub send:method {
		my ($self, $id, $value, $time) = @_;
		return carp "Need id and value" unless $id and defined $value;
		$time ||= int(time);
		my $buf = sprintf "%s %f %d\n", $id, $value, $time;
		#warn "send called $buf  ";
		if ( $self->{wsize} ) {
			#warn "pushed_to queue because of $self->{wsize}";
			$self->{wsize}++;
			my $l = { w => $buf };
			$self->{wlast}{next} = $l;
			$self->{wlast}       = $l;
			while ($self->{mem_limit} and $self->{wsize} > $self->{mem_limit}) {
				$self->{wbuf} = $self->{wbuf}{next};
				$self->{wsize}--;
			}
			#$self->_failover($buf) unless $self->{fh};
			$self->connect unless $self->{cnn};
			return;
		}
		if ($self->{fh}) {
			
			#my $w = syswrite( $self->{fh}, $buf );
			my $w = send( $self->{fh}, $buf, 0 );
			if ($w == length $buf) {
				#warn "sent $buf ";
				# ok
			}
			elsif (defined $w) {
				substr($buf,0,$w,'');
				$self->{wsize} = 1;
				$self->{wbuf} = $self->{wlast} = { w => $buf };
			}
			else {
				$self->{wsize} = 1;
				$self->{wbuf} = $self->{wlast} = { w => $buf };
				return $self->_disconnected("$!");
			}
			return;
			
		} else {
			#warn "not connected for $buf";
			$self->{wsize} = 1;
			$self->{wbuf} = $self->{wlast} = { w => $buf };
			$self->connect;
			
		}
	}
	
	sub _failover {
		my $self = shift;
		my $buf = shift;
		return unless $self->{failover};
		if (exists $self->{failover}{path}) {
			print { $self->{failover}{fh} } $buf
				or warn "Can't write to failover: $!";
		}
	}
	
	sub connect {
		my $self = shift;
		#warn "connection";
		if ($self->{type} eq 'udp') {
			$self->{cnn} = udp_connect $self->{host},$self->{port}, sub {
				$self or return;
				if (my $fh = shift) {
					warn "connected: @_";
					$self->{fh} = $fh;
					$self->_wio()->();
				} else {
					warn "connect failed: $!";
					$self->{cnn} = AE::timer 1,0,sub {
						$self or return;
						$self->connect;
					};
				}
			};
		} else {
			$self->{cnn} = tcp_connect $self->{host},$self->{port}, sub {
				$self or return;
				if (my $fh = shift) {
					warn "connected: @_";
					$self->{fh} = $fh;
					$self->_wio()->();
				} else {
					warn "connect failed: $!";
					$self->{cnn} = AE::timer 1,0,sub {
						$self or return;
						$self->connect;
					};
				}
			};
		}
	}
	
	sub stop {
		
	}


}

package main;

use EV;
use uni::perl ':dumper';
use AnyEvent::HTTP;
use Time::HiRes 'time';
use JSON::XS;
use AnyEvent::Socket;
use AnyEvent::Handle;

use constant ();
our %const;
BEGIN {
	
	%const = (
    CS_MAGIC      => 0xDEADBEEF,
    PROTO_VERSION => 0x10008,

    MRIM_CS_HELLO     => 0x1001,    ## C->S, empty
    MRIM_CS_HELLO_ACK => 0x1002,    ## S->C, UL mrim_connection_params_t

    MRIM_CS_LOGIN2    => 0x1038,    ## C->S, LPS login, LPS password, UL status, LPS useragent
    MRIM_CS_LOGIN_ACK => 0x1004,    ## S->C, empty
    MRIM_CS_LOGIN_REJ => 0x1005,    ## S->C, LPS reason
    MRIM_CS_LOGOUT    => 0x1013,    ## S->C, UL reason

    MRIM_CS_PING => 0x1006,         ## C->S, empty

    MRIM_CS_USER_STATUS      => 0x100f,     ## S->C, UL status, LPS user
    STATUS_OFFLINE           => 0x00000000,
    STATUS_ONLINE            => 0x00000001,
    STATUS_AWAY              => 0x00000002,
    STATUS_UNDETERMINED      => 0x00000003,
    MRIM_CS_USER_INFO        => 0x1015,
    MRIM_CS_ADD_CONTACT      => 0x1019,     # C->S UL flag, UL group_id, LPS email, LPS name
    CONTACT_FLAG_VISIBLE     => 0x00000008,
    CONTACT_FLAG_REMOVED     => 0x00000001,
    CONTACT_FLAG_SMS         => 0x00100000,
    MRIM_CS_ADD_CONTACT_ACK  => 0x101A,
    CONTACT_OPER_SUCCESS     => 0x00000000,
    CONTACT_OPER_USER_EXISTS => 0x00000005,
    MRIM_CS_AUTHORIZE        => 0x1020,     # C -> S, LPS user
    MRIM_CS_MODIFY_CONTACT   => 0x101B,     # C -> S, UL id, UL flags, UL group_id, LPS email, LPS name, LPS unused
    MRIM_CS_MODIFY_CONTACT_ACK => 0x101C,
    MRIM_CS_AUTHORIZE_ACK      => 0x1021,    # C -> S, LPS user

    MRIM_CS_MESSAGE                => 0x1008,       ## C->S, UL flags, LPS to, LPS message, LPS rtf-message
    MESSAGE_FLAG_OFFLINE           => 0x00000001,
    MESSAGE_FLAG_NORECV            => 0x00000004,
    MESSAGE_FLAG_AUTHORIZE         => 0x00000008,
    MESSAGE_FLAG_SYSTEM            => 0x00000040,
    MESSAGE_FLAG_RTF               => 0x00000080,
    MESSAGE_FLAG_NOTIFY            => 0x00000400,
    MESSAGE_FLAG_UNKOWN            => 0x00100000,
    MRIM_CS_MESSAGE_RECV           => 0x1011,
    MRIM_CS_MESSAGE_STATUS         => 0x1012,       # S->C
    MRIM_CS_MESSAGE_ACK            => 0x1009,       #S->C
    MRIM_CS_OFFLINE_MESSAGE_ACK    => 0x101D,       #S->C UIDL, LPS message
    MRIM_CS_DELETE_OFFLINE_MESSAGE => 0x101E,       #C->S UIDL

    MRIM_CS_CONNECTION_PARAMS => 0x1014,            # S->C

    MRIM_CS_CHANGE_STATUS    => 0x1022,
    MRIM_CS_GET_MPOP_SESSION => 0x1024,
    MRIM_CS_MPOP_SESSION     => 0x1025,

    MRIM_CS_ANKETA_INFO    => 0x1028,  # S->C
    MRIM_CS_WP_REQUEST     => 0x1029,  # C->S
    MRIM_CS_MAILBOX_STATUS => 0x1033,
    MRIM_CS_CONTACT_LIST2  => 0x1037,  # S->C UL status, UL grp_nb, LPS grp_mask, LPS contacts_mask, grps, contacts

    MRIM_CS_SMS     => 0x1039,         # C->S UL unkown, LPS number, LPS message
    MRIM_CS_SMS_ACK => 0x1040,         # S->C UL status

    # Don't look for file transfer, it's simply not handled
    # Mail.Ru only partially documented the old, unused P2P file transfer
    # the new file transfer simply gives an (unusable) RFC1918 address
    # when getting the MRIM_CS_FILE_TRANSFER packet
    # Needs some reverse-engineering. Has been done by Miranda's MRA plugin guys,
    # but for some reason i can't find the source

    MRIMUA => "Net::MRIM.pm v. "
	
	);
	constant->import(\%const);
	%const = reverse %const;
}

my $JSON = JSON::XS->new()->utf8->pretty;
use Data::Dumper;$Data::Dumper::Useqq = 1;

sub lps($) {  pack "V/a*", $_[0]; }

sub sendsms {
	my $mesg = shift;
tcp_connect 'mrim34.mail.ru', 443, sub {
	pop;
	my $fh = shift or die "$!";
	binmode $fh, ':raw';
	my $h;$h = AnyEvent::Handle->new(
		fh => $fh,
		on_error => sub {
			warn "$h: @_";
		},
		timeout => 0,
	);
	warn "connected @_";
	my $pk = pack( "V7", CS_MAGIC, PROTO_VERSION, 0, MRIM_CS_HELLO, 0, 0, 0 ).pack( "C[16]", 0 );
	warn "send packet of length ".length($pk)." ".Dumper($pk);
	$h->push_write($pk);
	my $reader;$reader = sub {
		$h->push_read(chunk => 44, sub {
			shift;
			my $hd = shift;
			my ( $magic, $proto, $seq, $msg, $dlen, $from, $from_port, $r1, $r2, $r3, $r4 ) = unpack( "V11", $hd  );
			warn "receive: $const{$msg} + $dlen ".Dumper $hd;
			#warn Dumper [$magic, $proto, $seq, $msg, $dlen, $from, $from_port, $r1, $r2, $r3, $r4];
			
			if ($dlen) {
				$h->unshift_read( chunk => $dlen, sub {
					shift;
					warn "receive data: ".Dumper $_[0];
					if ($msg == MRIM_CS_HELLO_ACK) {
						my $data = lps('ae-mrim@mail.ru') . lps('testing1') . pack( "V", 1 ) . lps( 'AE::MRIM 0.01' );
						my $pk = pack( "V7", CS_MAGIC, PROTO_VERSION, 1, MRIM_CS_LOGIN2, length($data), 0, 0 ).pack( "C[16]", 0 ).$data;
						warn "send packet of length ".length($pk)." ".Dumper($pk);
						$h->push_write($pk);
						return;
					}
					if ($msg == MRIM_CS_CONTACT_LIST2) {
						my $data = pack('V',0).lps('+79262309805') . lps("winston: $mesg");
						my $pk = pack( "V7", CS_MAGIC, PROTO_VERSION, 1, MRIM_CS_SMS, length($data), 0, 0 ).pack( "C[16]", 0 ).$data;
						
						warn "send packet of length ".length($pk)." ".Dumper($pk);
						$h->push_write($pk);
						my $wt;$wt = AE::timer 3,0,sub {
							undef $wt;
							undef $h;
							undef $reader;
							close $fh;
						};
						return;
						
					}
				});
			} else {
				warn "No data";
			}
			$reader->();
		});
	};
	$reader->();
};
return;

}



#my $g = My::Graphite->new( type => 'udp' );
my $g = My::Graphite->new( type => 'tcp' );
my $lastsent = time;
$| = 1;
my $t; $t = AE::timer 0,10, sub {
	$g,$t;
	http_request
		GET => 'http://www.winston.ru/static/wsf/prizes.json?_='.int(time * 1000),
		sub => sub {eval {
			my$json = $JSON->decode( $_[0] );
			#warn $JSON->encode($json);
			my @send;
			print ".";
			for my $p (@{ $json->{prize} }) {
				next if $p->{prizeSystemName} =~ /Reserved|LevelUp/;
				$g->send("winston.".$p->{prizeSystemName}, $p->{countLeft});
				if ($p->{countLeft} > 0) {
					print localtime()." $p->{prizeSystemName}: $p->{countLeft}\n";
					push @send, $p->{countLeft};
				}
			}
			if (@send and time - $lastsent > 1800 ) {
				sendsms("@send");
				$lastsent = time;
			}
		}};
		
};


EV::loop;

__END__

		my $write = sub {
			$self and exists $self->{$id} or return;
			for my $buf (@_) {
				ref $buf or do { $buf = \( my $str = $buf ); warn "Passed nonreference buffer from @{[ (caller)[1,2] ]}\n"; };
				#warn "Write $$buf";
				if ( $self->{$id}{wbuf} ) {
					$self->{$id}{closeme} and return warn "Write ($$buf) called while connection close was enqueued at @{[ (caller)[1,2] ]}";
					${ $self->{$id}{wbuf} } .= defined $$buf ? $$buf : return $self->{$id}{closeme} = 1;
					return;
				}
				elsif ( !defined $$buf ) { return $drop->($id); }
				
				my $w = syswrite( $self->{$id}{fh}, $$buf );
				if ($w == length $$buf) {
					# ok;
				}
				elsif (defined $w) {
					substr($$buf,0,$w,'');
					$self->{$id}{wbuf} = $buf;
					$self->{$id}{ww} = AE::io $self->{$id}{fh}, 1, sub {
						#warn "ww.io.$id";
						$self and exists $self->{$id} or return;
						$w = syswrite( $self->{$id}{fh}, ${ $self->{$id}{wbuf} } );
						if ($w == length ${ $self->{$id}{wbuf} }) {
							delete $self->{$id}{wbuf};
							delete $self->{$id}{ww};
							if( $self->{$id}{closeme} ) { $drop->($id); }
						}
						elsif (defined $w) {
							${ $self->{$id}{wbuf} } = substr( ${ $self->{$id}{wbuf} }, $w );
							#substr( ${ $self->{$id}{wbuf} }, 0, $w, '');
						}
						else { return $drop->($id, "$!"); }
					};
				}
				else { return $drop->($id, "$!"); }
			}
		};
