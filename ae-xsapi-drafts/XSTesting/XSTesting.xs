/*
 * Usage:
 
 my $x = XSTesting::makecb("my arg for x");
 my $y = XSTesting::makecb("my arg for y");
 say $x->();
 say $y->();

*/

#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include "ppport.h"

XS(cb_test) {
	dVAR;
	dXSARGS;
	dORIGMARK;
	
	SV *var = (SV *) CvXSUBANY(cv).any_ptr;
	
	//SP = ORIGMARK;
	
	ST(0) = var;
	XSRETURN(1);
}

MODULE = XSTesting		PACKAGE = XSTesting

void makecb(SV *var)
	PPCODE:
		CV *myxs = newXS(0, cb_test, __FILE__);
		void *context = (void *) var;
		
		// Currently I have an ideas:
		// - Assign context ptr to magic of CV; Something like:
		//		sv_magicext((SV *) myxs, NULL, PERL_MAGIC_ext, &null_mg_vtbl, context, 0);
		// - Create PADLIST for CV and store context there
		//		how?
		// - Just use any_ptr
		CvXSUBANY(myxs).any_ptr = (void *) var;
		
		ST(0) = sv_2mortal( newRV_noinc( (SV *) myxs ) );
		//warn("cv = %p, mysv = %p",myxs, var);
		XSRETURN(1);
