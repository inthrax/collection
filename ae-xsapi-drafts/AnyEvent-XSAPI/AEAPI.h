
//#define dAEIO SvPVX( ((SV *) CvXSUBANY(cv).any_ptr) )
#define dAEAPI(x) ae_ ## x * w = ((ae_ ## x *) CvXSUBANY(cv).any_ptr)

// cc - is C callback
#define AE_P_

# define AE_CB_DECLARE(type) void (*cb) (AE_P_ struct type *, int);

static int AE_USE_EV = 0;

struct ae_io_;
typedef struct ae_io_ ae_io;

typedef void (*ae_io_cc)( ae_io *, int );


struct ae_io_ {
#ifdef WITH_EV
	ev_io ev;
#else
#endif
	ae_io_cc cb;
	SV *w; //watcher
	SV *fh;
};


XS(AEAPIxs2cc_io) {
	dXSARGS;
	dAEAPI(io);
	w->cb( w, 0 );
	XSRETURN_UNDEF;
}

void ae_io_cv( ae_io * io, SV * fd, int flag_read_write, CV * cb ) {
	dSP;
	dVAR;
	
	warn("call ae::io on %p",io);
	CvXSUBANY(cb).any_ptr = (void *) io;
	
	ENTER;
	SAVETMPS;
	
	PUSHMARK(SP);
	EXTEND(SP,3);
	PUSHs( fd );
	PUSHs( newSViv(0) );
	PUSHs( newRV_noinc( (SV *) cb ) );
	PUTBACK;
	
	int count = call_pv( "AE::io", G_SCALAR );
	
	SPAGAIN;
	
	SvREFCNT_inc( SvAEIO( io )->w = POPs );
	
	warn("count=%d, i=%s", count, SvPV_nolen( SvAEIO( io )->w  ) );
	
	PUTBACK;
	FREETMPS;
	LEAVE;
}

void ae_io_xs( ae_io_t io, SV * fd, int flag_read_write, XSUBADDR_t xsub ) {
	ae_io_cv( io, fd, flag_read_write, newXS(0, xsub, __FILE__) );
}


void ae_io_cb( ae_io *io, SV * fd, int flag_read_write, ae_io_cc cb ) {
#ifdef WITH_EV
	if (AE_USE_EV) {
		ev_io_init( io->ev, cb, extract_fileno(fd), flag_read_write);
		ev_io_start(EV_DEFAULT, io->ev)
	} else
#endif
	{
		io->cb = cb;
		ae_io_cv(io, fd, flag_read_write, newXS(0, AEAPIxs2cc_io, __FILE__));
	}
}


static ae_io * ae_io_new() {
	return ( (ae_io *) safemalloc(sizeof(ae_io)) );
}
