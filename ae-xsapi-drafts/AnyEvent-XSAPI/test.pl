#!/usr/bin/env perl

{
	package MYOUT;
	
	sub TIEHANDLE {
		#warn "@_";
		my $pk = shift;
		my $fd = shift;
		open my $dup, '>&', $fd;
		close $fd;
		return bless \$dup, $pk;
	}
	
	sub PRINT {
		warn "called PRINT: @_";
		
		${$_[0]}->print(@_[ 1..$#_ ]);
	}
	
	sub AUTOLOAD {}
}

BEGIN {
	tie *STDOUT, 'MYOUT', *STDOUT;
}

close STDOUT;

print 1;


__END__
use AnyEvent::Impl::Perl;
use AnyEvent;

print fileno(STDIN),"\n";

my $io; $io = AE::io \*STDIN,0,sub {
	warn "ready";
	sysread(STDIN,my $buf, 4096);
	print $buf;
	undef $io;
};

AE::cv->recv;
undef $io;