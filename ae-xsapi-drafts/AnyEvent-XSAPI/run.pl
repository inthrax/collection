#!/usr/bin/env perl
use uni::perl ':dumper';
no warnings qw(internal FATAL);
use warnings qw(internal);
use Data::Dumper;
use Devel::Hexdump 'xd';
use ExtUtils::testlib;
use Devel::Peek;
use Devel::Leak;
use AnyEvent::Impl::Perl;
#use EV;
use AnyEvent;
use AnyEvent::XSAPI;


use Scalar::Util 'weaken';
use Time::HiRes 'sleep';

AnyEvent::XSAPI::readline2(\*STDIN);

AE::cv->recv;
#EV::loop;

__END__
while () {
#Devel::Leak::NoteSV(my $h);
for (1..100) {
	my $xx = XSTesting::makecb("my arg for xx");
	$xx->();
	undef $xx;
}
#Devel::Leak::CheckSV($h);

#Dump $xx;
#weaken(my $xc = $xx);
#Dump $xc;
}
#Dump $xc;
__END__
sub mk {
	my $z = XSTesting::makecb("my arg for x");
	return $z;
}

Devel::Leak::NoteSV(my $h);

my $x = mk();
#my $y = XSTesting::makecb("my arg for y");
#warn "cb=$x (+$y)";
#Dump mk("test1");
#Dump mk("test2");
#Dump $x;
say $x->() for 1..10;
#say $y->();
weaken(my $xc = $x);
Dump $xc;
undef $x;
#undef $y;

#Dump $y;
#Devel::Leak::CheckSV($h);

undef $xx;