#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include "ppport.h"
#include "AEAPI.h"
/*
#define create_callback( xscv, xsub, context ) STMT_START {  xscv = newXS(0, xsub, __FILE__); CvXSUBANY(xscv).any_ptr = (void *) context;  } STMT_END
//#define dAEIO SvPVX( ((SV *) CvXSUBANY(cv).any_ptr) )
#define dAEIOXS ae_io_t io = ((SV *) CvXSUBANY(cv).any_ptr)

typedef SV* ae_io_t;

typedef void (*ae_io_cc)( ae_io_t w );

typedef struct {
	//int  fd;
	SV  *w; //watcher
	ae_io_cc ccb;
} ae_io_inner;



#define SvAEIO( aesv ) ((ae_io_inner *)SvPVX( aesv ))

ae_io_t ae_io_new() {
	SV *sv = newSV( sizeof(ae_io_inner) );
	SvPOK_only( sv );
	SvCUR_set(sv, sizeof(ae_io_inner));
	//myww *this = (myww *) SvPVX( sv );
	return (ae_io_t) sv;
}


XS(cc_xs_call) {
	dXSARGS;
	dAEIOXS;
	
	SvAEIO(io)->ccb( io );
	//( (ae_io_cc) CvXSUBANY(cv).any_ptr )( io );
	
	XSRETURN_UNDEF;
}

void ae_io_cv( ae_io_t io, SV * fd, int flag_read_write, CV * cb ) {
	dSP;
	dVAR;
	
	warn("call ae::io on %p",io);
	CvXSUBANY(cb).any_ptr = (void *) io;
	
		ENTER;
		SAVETMPS;
		
		PUSHMARK(SP);
		EXTEND(SP,3);
		PUSHs( fd );
		PUSHs( newSViv(0) );
		PUSHs( newRV_noinc( (SV *) cb ) );
		PUTBACK;
		
		int count = call_pv( "AE::io", G_SCALAR );
		
		SPAGAIN;
		
		SvREFCNT_inc( SvAEIO( io )->w = POPs );
		
		warn("count=%d, i=%s", count, SvPV_nolen( SvAEIO( io )->w  ) );
		
		PUTBACK;
		FREETMPS;
		LEAVE;
}

void ae_io_xs( ae_io_t io, SV * fd, int flag_read_write, XSUBADDR_t xsub ) {
	//dXSARGS;
	
	//warn("xxxxxxxx: %d / %d", fd, fileno(fd));
		CV *cb = newXS(0, xsub, __FILE__);
		ae_io_cv(io, fd, flag_read_write, cb);
}

void ae_io_sv( ae_io_t io, int fileno, int flag_read_write, SV * cb ) {
	
}

void ae_io_cb( ae_io_t io, SV * fd, int flag_read_write, ae_io_cc cb ) {
	
	CV *myxs = newXS(0, cc_xs_call, __FILE__);
	SvAEIO(io)->ccb = cb;
	//CvXSUBANY(myxs).any_ptr = (void *) io;
	ae_io_cv(io,fd,flag_read_write, myxs);
	
}

void ae_io_free(ae_io_t io) {
	ae_io_inner * i = SvAEIO( io );
	
	if (i->w)
		SvREFCNT_dec(i->w);
	i->w = 0;
	
	SvREFCNT_dec( (SV *) io );
}

typedef struct {
	SV *w;
} myww;


XS(cb_test) {
	dVAR;
	dXSARGS;
	dORIGMARK;
	
	SV *var = (SV *) CvXSUBANY(cv).any_ptr;
	
	//SP = ORIGMARK;
	
	ST(0) = var;
	XSRETURN(1);
}

XS(cb_io) {
	dVAR;
	dXSARGS;
	dORIGMARK;
	
	
	SV *sv = (SV *) CvXSUBANY(cv).any_ptr;
	myww *this = (myww *) SvPVX( sv );
	
	//SP = ORIGMARK;
	
	warn("cb io");
	SvREFCNT_dec(this->w);
	this->w = 0;
	
	XSRETURN_UNDEF;
}

XS(cb_io_2) {
	//dVAR;
	
	dXSARGS;
	
	//dORIGMARK;
	
	dAEIOXS;
	
	//SP = ORIGMARK;
	
	warn("cb 2 io: %p", io);
	ae_io_free(io);
	
	XSRETURN_UNDEF;
}

void mycbx( ae_io_t io ) {
	warn("cb 3 io: %p", io);
	ae_io_free(io);
}

*/

MODULE = AnyEvent::XSAPI		PACKAGE = AnyEvent::XSAPI

void readline2(SV *fh)
	PPCODE:
		ae_io_t io = ae_io_new();
		//ae_io_xs( io, fh, 0, cb_io_2 );
		ae_io_cb( io, fh, 0, mycbx );

void readline(SV *fh)
	PPCODE:
		CV *cb = newXS(0, cb_io, __FILE__);
		
		SV *sv = newSV( sizeof(myww) );
		SvPOK_only( sv );
		SvCUR_set(sv, sizeof(myww));
		myww *this = (myww *) SvPVX( sv );
		
		CvXSUBANY(cb).any_ptr = (void *) sv;
		
		ENTER;
		SAVETMPS;
		
		PUSHMARK(SP);
		EXTEND(SP,3);
		PUSHs(fh);
		PUSHs( newSViv(0) );
		PUSHs( newRV_noinc( (SV *) cb ) );
		PUTBACK;
		
		int i,count = call_pv( "AE::io", G_SCALAR );
		
		SPAGAIN;
		
		SV *rv = POPs;
		SvREFCNT_inc( rv );
		this->w = rv;
		
		warn("Result: %d, %s",count, SvPV_nolen(rv));
		
		PUTBACK;
		FREETMPS;
		LEAVE;
		

void makecb(SV *var)
	PPCODE:
		CV *myxs = newXS(0, cb_test, __FILE__);
		void *context = (void *) var;
		
		// Currently I have an ideas:
		// - Assign context ptr to magic of CV; Something like:
		//		sv_magicext((SV *) myxs, NULL, PERL_MAGIC_ext, &null_mg_vtbl, context, 0);
		// - Create PADLIST for CV and store context there
		//		how?
		// - Just use any_ptr
		CvXSUBANY(myxs).any_ptr = (void *) var;
		
		ST(0) = sv_2mortal( newRV_noinc( (SV *) myxs ) );
		//warn("cv = %p, mysv = %p",myxs, var);
		XSRETURN(1);
