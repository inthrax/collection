#!/usr/bin/env perl
use uni::perl ':dumper';
no warnings qw(internal FATAL);
use warnings qw(internal);
use Data::Dumper;
use ExtUtils::testlib;
use Devel::Peek;
use Devel::Leak;

use AnyEvent::XS;

#Devel::Leak::NoteSV(my $handle);

my $x = AnyEvent::XS::test("my arg");
warn "res: $x";

my $y = sub {};

Dump $x;
Dump $y;

sub inner {
	Carp::cluck "called: @_"; 
}

sub test {
	say $x->(\&inner, 1,2,3);
	say $x->(\&inner, 1,2,3);
}

test();

undef $x;

#Devel::Leak::CheckSV($handle);


__END__
#use EV;
use AnyEvent::Impl::Perl;
use AnyEvent;

if (EV) {
	use AE::XS::EV;
		I_USE_EV(...);
}
else {
	use AE::XS::PP;
		
}

say AnyEvent::detect();
