#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include "ppport.h"
#include <stdint.h>

/*
#define debug_print1(x) STMT_START {{\
	fprintf(stderr, "%s:%d: ", __FILE__, __LINE__);\
	printf x;\
}} STMT_END
*/

# define debug_print(x) printf x

char *
cxtype_name(U32 cx_type_arg)
{
  switch(cx_type_arg & CXTYPEMASK)
  {
    case CXt_NULL:   return "null";
    case CXt_SUB:    return "sub";
    case CXt_EVAL:   return "eval";
    //case CXt_LOOP:   return "loop";
    case CXt_SUBST:  return "subst";
    case CXt_BLOCK:  return "block";
    case CXt_FORMAT: return "format";

    default:        debug_print(("Unknown context type 0x%lx\n", cx_type_arg));
                                         return "(unknown)";
  }
}

void
show_cxstack(void)
{
    I32 i;
    for (i = cxstack_ix; i>=0; --i)
    {
        printf(" =%ld= %s (%lx)", (long)i,
            cxtype_name(CxTYPE(&cxstack[i])), cxstack[i].blk_oldcop->cop_seq);
        if (CxTYPE(&cxstack[i]) == CXt_SUB) {
              CV *cv = cxstack[i].blk_sub.cv;
              printf("\t%s", (cv && CvGV(cv)) ? GvNAME(CvGV(cv)) :"(null)");
        }
        printf("\n");
    }
}

I32
dopoptosub_at(pTHX_ PERL_CONTEXT *cxstk, I32 startingblock)
{
    dTHR;
    I32 i;
    PERL_CONTEXT *cx;
    for (i = startingblock; i >= 0; i--) {
        cx = &cxstk[i];
        switch (CxTYPE(cx)) {
        default:
            continue;
        case CXt_SUB:
        /* In Perl 5.005, formats just used CXt_SUB */
#ifdef CXt_FORMAT
       case CXt_FORMAT:
#endif
            debug_print(("**dopoptosub_at: found sub #%ld\n", (long)i));
            return i;
        }
    }
        debug_print(("**dopoptosub_at: not found #%ld\n", (long)i));
    return i;
}

I32
dopoptosub(pTHX_ I32 startingblock)
{
    dTHR;
    return dopoptosub_at(aTHX_ cxstack, startingblock);
}

/* This function is based on the code of pp_caller */
PERL_CONTEXT*
upcontext(pTHX_ I32 count, COP **cop_p, PERL_CONTEXT **ccstack_p,
                                I32 *cxix_from_p, I32 *cxix_to_p)
{
    PERL_SI *top_si = PL_curstackinfo;
    I32 cxix = dopoptosub(aTHX_ cxstack_ix);
    PERL_CONTEXT *ccstack = cxstack;

    if (cxix_from_p) *cxix_from_p = cxstack_ix+1;
    if (cxix_to_p)   *cxix_to_p   = cxix;
    for (;;) {
        /* we may be in a higher stacklevel, so dig down deeper */
        while (cxix < 0 && top_si->si_type != PERLSI_MAIN) {
            top_si  = top_si->si_prev;
            ccstack = top_si->si_cxstack;
            cxix = dopoptosub_at(aTHX_ ccstack, top_si->si_cxix);
                        if (cxix_to_p && cxix_from_p) *cxix_from_p = *cxix_to_p;
                        if (cxix_to_p) *cxix_to_p = cxix;
        }
        if (cxix < 0 && count == 0) {
                    if (ccstack_p) *ccstack_p = ccstack;
            return (PERL_CONTEXT *)0;
                }
        else if (cxix < 0)
            return (PERL_CONTEXT *)-1;
        if (PL_DBsub && cxix >= 0 &&
                ccstack[cxix].blk_sub.cv == GvCV(PL_DBsub))
            count++;
        if (!count--)
            break;

        if (cop_p) *cop_p = ccstack[cxix].blk_oldcop;
        cxix = dopoptosub_at(aTHX_ ccstack, cxix - 1);
                        if (cxix_to_p && cxix_from_p) *cxix_from_p = *cxix_to_p;
                        if (cxix_to_p) *cxix_to_p = cxix;
    }
    if (ccstack_p) *ccstack_p = ccstack;
    return &ccstack[cxix];
}


XS(cb_test) {
	dVAR;
	dXSARGS;
	dORIGMARK;

	warn("sub = %p", ST(0));
	//asm("int $3");
	I32 cxix = cxstack_ix;
	PERL_CONTEXT *ccstk = cxstack;
	PERL_CONTEXT *upcx = upcontext(0,0,0,0,0);
	warn("cxix = %d; cx=%p, ucx=%p", cxix, ccstk, upcx);
	//cx_dump(cxstack);
	show_cxstack();
	
	PUSHMARK(SP);
	EXTEND(SP, 1);
	PUSHs( ST(1) );
	PUTBACK;
	
	call_sv(ST(0), G_DISCARD | G_VOID );
	
	for (;;) {
		while( cxix >= 0 ) {
			PERL_CONTEXT *cx = &ccstk[cxix--];
			if (CxTYPE (cx) == CXt_SUB) {
				warn("sub");
				CV *cv = cx->blk_sub.cv;
				if (CvDEPTH (cv)) {
					warn ("cv = %p", cv);
				}
				else {
					warn("no depth");
				}
			}
			else
			if (CxTYPE (cx) == CXt_BLOCK) {
				//warn("block: %p", cx->blk_oldcop.cv);
				/*
				asm("int $3");
				CV *cv = cx->blk_sub.cv;
				if (CvDEPTH (cv)) {
					warn ("cv = %p", cv);
				}
				else {
					warn("no depth");
				}
				*/
				
			}
			else {
				warn("not sub");
			}
			//break;
		}
		//if ( top_si->si_type == PERLSI_MAIN)
			break;
	}
	
	
	
	//warn("curpad: %p, comppad: %p, PADSVl = %p", PL_curpad, PL_comppad, SvRV( PAD_SVl(1) ));
	//warn("padlist = %p",CvPADLIST( (CV *) SvRV( PAD_SVl(1) ) ));
	/*
	SAVECOMPPAD();
	PAD_SET_CUR_NOSAVE(CvPADLIST( (CV *) SvRV( PAD_SVl(1) ) ),1);
	
	SV *sv = PAD_SVl(1);
	warn("testing (%d) SV=<%s>...", items, SvPV_nolen(sv));
	*/
	warn("testing (%d) SV=<>...", items);
	
	//ST(0) = sv;
	//XSRETURN(1);
	
	SP = ORIGMARK;
	PUSHs(&PL_sv_yes);
}

MODULE = AnyEvent::XS		PACKAGE = AnyEvent::XS

void test(SV *var)
	PPCODE:
		CV *myxs = newXS(0, cb_test, __FILE__);
		
		
		//warn("curpad: %p, comppad: %p, myxs = %p, SVL1 = %s", PL_curpad, PL_comppad, myxs, SvPV_nolen( PAD_SVl(1) ));
		
		//CvPADLIST(myxs) = pad_new(padnew_SAVE|padnew_SAVESUB);
		/*
		//warn("padlist = %p",CvPADLIST(myxs));
		PADOFFSET off = pad_add_name_pvs("var", 0,0,0);
		PAD_SETSV( off, var );
		*/
		
		//warn("curpad: %p, comppad: %p, SVL1 = %s", PL_curpad, PL_comppad, SvPV_nolen( PAD_SVl(1) ));
		SV *cb = newRV_noinc( (SV *) myxs );
		warn ("created cb = %s", SvPV_nolen( cb ));
		ST(0) = cb;
		XSRETURN(1);
