#!/usr/bin/env perl

my $s = 'lua';
while (<>) {
	chomp();
	s/\s*--.*$//;
	next if /^\s*$/;
	s/^\s+//s;
	$s .= ' '.$_;
}

print $s."\n";
