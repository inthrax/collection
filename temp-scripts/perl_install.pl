#!/usr/bin/env perl

use strict;
use 5.010;
use Data::Dumper;

{
	my $envrc = "$ENV{HOME}/.envrc";
	open my $f, '>', $envrc or die "open $envrc failed: $!";
	print {$f} <<EOB
PERL_CPANM_OPT='--notest'; export PERL_CPANM_OPT;
EOB
;
	close $f;
}

{
	my $profile = "$ENV{HOME}/.profile";
	open my $f, '+<', $profile or die "open $profile failed: $!";
	my $lines = do { local $/; <$f> }; #???? local $/
	my @lines = split /\n/,$lines;
	my $have_envrc = 0;
	for (@lines) {
		next if /^\s*#/;
		next if /^\s*$/;
		if (/\s*PATH=/) {
			unless (m{(=\s*|:)\$HOME/bin:}) {
				s{(?<=PATH=)}{\$HOME/bin:};
				say;
			}
		}
		if (/\.envrc/) { $have_envrc = 1 }
	}
	unless ($have_envrc) {
		push @lines, '','. $HOME/.envrc';
	}
	seek $f,0,0 or die "Can't seek $profile";
	truncate $f,0 or die "Can't truncate $profile";
	print {$f} map {"$_\n"} @lines;
	close $f;
}

our $PERL = "$ENV{HOME}/bin/perl";

unless (-e $PERL) {
	my $perl_version='5.16.1';
	my $perl = "perl-$perl_version";
	mkdir "$ENV{HOME}/src";
	chdir "$ENV{HOME}/src" or die "Can't chdir to src: $!";
	my $perl_url = "http://www.cpan.org/src/5.0/$perl.tar.gz";
	-e $perl.'.tar.gz' or system("wget $perl_url") == 0 or die;
	if (-d $perl) {
		system ("rm -rf $perl") == 0 or die;
#		system("make clean") == 0 or die;
	}
	# else 
	{
		system("tar zxf $perl.tar.gz") == 0 or die;
	}
	chdir $perl or die "chdir to $perl: $!";
	my $HOME = $ENV{HOME};
	my $email = 'mons@rambler-co.ru';
system(	qq{sh Configure -des -Dprefix='$HOME' -Duselargefiles -Duse64bitint -DUSEMYMALLOC -DDEBUGGING -Dinc_version_list=none}.
		qq{ -Doptimize='-march=athlon64 -fomit-frame-pointer -pipe -ggdb -g3 -O2' -Dccflags='-DPIC -fPIC -O2 -march=athlon64 -fomit-frame-pointer -pipe -ggdb -g3'}.
		qq{ -D cf_email=$email -D perladmin=$email -D locincpth="${HOME}/include /usr/local/include"}.
		qq{ -D loclibpth="${HOME}/lib /usr/local/lib" -D privlib="${HOME}/lib/perl5/$perl_version" -D archlib="${HOME}/lib/perl5/$perl_version"}.
		qq{ -D sitelib="${HOME}/lib/perl5/$perl_version" -D sitearch="${HOME}/lib/perl5/$perl_version" -Uinstallhtml1dir=''}.
		qq{ -Uinstallhtml3dir='' -Uinstallman1dir='' -Uinstallman3dir='' -Uinstallsitehtml1dir='' -Uinstallsitehtml3dir='' -Uinstallsiteman1dir=''}.
		qq{ -Uinstallsiteman3dir='' && make install}) == 0

		or die "Can't build perl";
} else {
	print "Have installed home perl: ";
	system("$ENV{HOME}/bin/perl -E 'say \$^V'"); # say <= features
}

unless (-e "$ENV{HOME}/bin/cpanm") {
	say "Installing cpanm";
	system("curl -L http://cpanmin.us | $PERL - --self-upgrade");
} else {
	say "Have cpanm";
}

