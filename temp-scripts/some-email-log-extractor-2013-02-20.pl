#!/usr/bin/env perl

use strict;
use 5.010;
my %see;
my %dom;
$| = 1;

open my $all, '>:raw','00-rand.eml' or warn("Can't open 00-rand.eml: $!");

our $last;
$SIG{INT} = sub { $last = 1 };

for (<*.log>) {
	open my $f,'<:raw',$_ or warn("Can't open $_: $!"),next;
	say "opened $_";
	my $count = 0;
	my $dups = 0;
	while (<$f>) {
		last if $last;
		if (m{\b((\S+)@([a-z0-9_\.-]+))\b}) {
			#say $1; next;
			if ($see{ $1 }++) {
				$dups++;
			} else {
				$count++;
				$dom{$3}++;
				print { $all } "$1\n";
			}
			if ($count % 1000 == 0) {
				my @top = map { "$_:$dom{$_}" } ( sort { $dom{$b} <=> $dom{$a} } keys %dom )[0..9];
				print + (' 'x60)."\r$count ($dups) @top";
				#last if $count >= 1000;
			}
		}
	}
	my @top = map { "$_:$dom{$_}" } ( sort { $dom{$b} <=> $dom{$a} } keys %dom )[0..9];
	print + (' 'x60)."\r$count ($dups) @top\n";
	
	last if $last;
}

close $all;

my %fdd;
open my $sorted, '>:raw','00-all.eml' or warn("Can't open 00-all.eml: $!");

my $rank = 0;
for ( my @top = ( sort { $dom{$b} <=> $dom{$a} } keys %dom )[0..31] ) {
	my $fn = sprintf "%02d-%s.eml", ++$rank,$_;
	open my $fd, '>:raw',$fn or warn("Can't open $fn: $!"),next;
	$fdd{$_} = $fd;
}

for (sort keys %see) {
	if (m{([^@]+)@([^@]+)}) {
		if ($fdd{$2}) {
			print { $fdd{$2} } "$_\n";
		}
		print { $sorted } "$_\n";
	}
}

close $_ for $sorted, values %fdd