#!/usr/bin/env perl

use 5.016;
use HTTP::Easy::Status;
use Data::Dumper;

my @av;
my %words;

#$w[0]

for ( sort { $a <=> $b } keys %HTTP::Easy::Status::MSG ) {
	my $cls = int( $_ / 100 )-1;
	my $pos = ( $_ % 100 );
	#next if $pos == 99 or $pos == 49;
	$av[$cls][$pos] = $HTTP::Easy::Status::MSG{$_};
	my $msg = $HTTP::Easy::Status::MSG{$_};
	$words{$_}++ for split /\s+/, $msg;
	#print "$cls/$pos \n";
}

my @w;my %w;
for (sort keys %words) {
	if ( $words{$_} < 2 or length() < 5 ) {
		delete $words{$_};
		next;
	};
	push @w, $_;
	$w{$_} = $#w;
}
#warn Dumper \@w, \%w;

print "my %HTTP = do {\n";
print "\tlocal \$a,\$b;\n";
print "\tmy \@w = qw(@w);\n";
print "\t".q/map { ++$a;$b=0;map+(100*$a+$b++=>$_)x(!!$_),@$_; }/."\n";

for (@av) {
	print "\t\t[";
	
	my $skip;
	for (@$_) {
		if (!defined) {
			$skip++;
		}
		else {
			s{(\w+)}{ exists $w{$1} ? '$w['.$w{$1}.']' : $1 }sge;
			if ($skip) {
				if ( $skip > 3 ) {
					print "(0)x$skip,";
				} else {
					print "0,"x$skip;
				}
			}
			print qq{"$_",};
			$skip = 0;
		}
		#print defined() ? "'$_'" : 0, ",";
	}
	print "],\n";
}
print "};\n";

#print Dumper \@av;