#!/usr/bin/env perl

use strict;
use AnyEvent;
use AnyEvent::Socket;
use Scalar::Util 'refaddr', 'weaken';
use Errno qw(EAGAIN EINTR);
use AnyEvent::Util qw(WSAEWOULDBLOCK guard);
use Compress::Zlib;
sub MAX_READ_SIZE () { 128 * 1024 }
sub DEBUG () { 0 }

sub http_server($$&) {
	my $LF = "\015\012";
	my @hdr = map { lc $_ }
	my @hdrn  = qw(Upgrade Connection Content-Type WebSocket-Origin WebSocket-Location Sec-WebSocket-Origin Sec-Websocket-Location Sec-WebSocket-Key Sec-WebSocket-Accept Sec-WebSocket-Protocol);
	my %hdr; @hdr{@hdr} = @hdrn; my %hdri; @hdri{ @hdr } = 0..$#hdr;
	my %http = do {
		local $a,$b;
		my @w = qw(Content Entity Error Failed Found Gateway Large Proxy Request Required Timeout);
		map { ++$a;$b=0;map+(100*$a+$b++=>$_)x(!!$_),@$_; }
			["Continue","Switching Protocols","Processing",],
			[qw(OK Created Accepted),"Non-Authoritative Information","No $w[0]","Reset $w[0]","Partial $w[0]","Multi-Status",],
			["Multiple Choices","Moved Permanently","$w[4]","See Other","Not Modified","Use $w[7]",0,"Temporary Redirect",],
			["Bad $w[8]","Unauthorized","Payment $w[9]","Forbidden","Not $w[4]","Method Not Allowed","Not Acceptable","$w[7] Authentication $w[9]","$w[8] $w[10]","Conflict","Gone","Length $w[9]","Precondition $w[3]","$w[8] $w[1] Too $w[6]","$w[8]-URI Too $w[6]","Unsupported Media Type","$w[8] Range Not Satisfiable","Expectation $w[3]",(0)x4,"Unprocessable $w[1]","Locked","$w[3] Dependency","No code","Upgrade $w[9]",(0)x22,"Retry with",],
			["Internal Server $w[2]","Not Implemented","Bad $w[5]","Service Unavailable","$w[5] $w[10]","HTTP Version Not Supported","Variant Also Negotiates","Insufficient Storage",0,"Bandwidth Limit Exceeded","Not Extended",(0)x88,"Client $w[2]",],
	};
	my $ico = Compress::Zlib::memGunzip pack "H*","1f8b08000000000000ff636060044201010620a9c090c1c2c020c6c0c0a001c44021a008441c0c807242dc100c03ffffff1f1418e2144c1a971836fd308c4f3f08373434609883ac06248fac161b9b16fe47772736bfe1b29f1efa89713f363b08d98d1ceec4b89f5cfd84dc8f4f3f480e19131306a484ffc0610630beba9e81e1e86206860bcc10fec966289ecfc070b01d48b743d820b187cd0c707d0004091d8c7e040000";
	$ico = "HTTP/1.1 200 OK${LF}Connection:close${LF}Content-Type:image/x-icon${LF}Content-Length:".length($ico)."${LF}${LF}".$ico;
	
	{
		package __HTTP__Req;
		use strict;
		
		sub method  { $_[0][0] }
		sub uri     { $_[0][1] }
		sub headers { $_[0][2] }
		
		sub reply {
			my ($self,$code,$content,%args) = @_;
			$code ||=200;
			utf8::encode $content if utf8::is_utf8 $content;
			my $reply = "HTTP/1.0 $code $http{$code}$LF";
			my @good;my @bad;
			my $h = {
				%{ $args{headers} || {} },
				'connection' => 'close',
				'content-length' => length($content),
			};
			for (keys %$h) {
				if (exists $hdr{lc $_}) { $good[ $hdri{lc $_} ] = $hdr{ lc $_ }.": ".$h->{$_}.$LF; }
				else { push @bad, "\u\L$_\E: ".$h->{$_}.$LF; }
			}
			defined() and $reply .= $_ for @good,@bad;
			$reply .= $LF.$content;
			$self->[3] and (delete $self->[3])->( \$reply, \undef );
		}
		sub DESTROY {
			my $self = shift;
			$self->[3] and $self->[3]->(\("HTTP/1.0 404 Not Found\nConnection:close\nContent-type:text/plain\n\nRequest not handled\n"), \undef);
			@$self = ();
		}
	}
	
	my ($lhost,$lport,$reqcb) = @_;
	
	my $self = {
		backlog   =>  1024,
		read_size =>  4096,
	};
	my $drop = sub {
		my $id = shift;
		$self or return;
		#warn "Dropping connection $id: @_";
		$self->{connections}--;
		%{ delete $self->{$id} } = ();
	};
	
	$self->{listen} = tcp_server $lhost,$lport,sub {
		$self->{connections}++;
		my ($fh,$host,$port) = @_;
		my $id = ++$self->{seq}; #refaddr $fh;
		my %r = ( fh => $fh, wq => [], id => $id );
		$self->{ $id } = \%r;
		
		my $write = sub {
			$self and exists $self->{$id} or return;
			for my $buf (@_) {
				ref $buf or do { $buf = \( my $str = $buf ); warn "Passed nonreference buffer from @{[ (caller)[1,2] ]}\n"; };
				#warn "Write $$buf";
				if ( $self->{$id}{wbuf} ) {
					$self->{$id}{closeme} and return warn "Write ($$buf) called while connection close was enqueued at @{[ (caller)[1,2] ]}";
					${ $self->{$id}{wbuf} } .= defined $$buf ? $$buf : return $self->{$id}{closeme} = 1;
					return;
				}
				elsif ( !defined $$buf ) { return $drop->($id); }
				
				my $w = syswrite( $self->{$id}{fh}, $$buf );
				if ($w == length $$buf) {
					# ok;
				}
				elsif (defined $w) {
					substr($$buf,0,$w,'');
					$self->{$id}{wbuf} = $buf;
					$self->{$id}{ww} = AE::io $self->{$id}{fh}, 1, sub {
						#warn "ww.io.$id";
						$self and exists $self->{$id} or return;
						$w = syswrite( $self->{$id}{fh}, ${ $self->{$id}{wbuf} } );
						if ($w == length ${ $self->{$id}{wbuf} }) {
							delete $self->{$id}{wbuf};
							delete $self->{$id}{ww};
							if( $self->{$id}{closeme} ) { $drop->($id); }
						}
						elsif (defined $w) {
							${ $self->{$id}{wbuf} } = substr( ${ $self->{$id}{wbuf} }, $w );
							#substr( ${ $self->{$id}{wbuf} }, 0, $w, '');
						}
						else { return $drop->($id, "$!"); }
					};
				}
				else { return $drop->($id, "$!"); }
			}
		};
		
		my %s;
		$r{rw} = AE::io $fh, 0, sub {
			#warn "rw.io.$id";
			$self and exists $self->{$id} or return;
			
			my $buf = $self->{$id}{rbuf};my $len;
			while ( $self and ( $len = sysread( $fh, $buf, $self->{read_size}, length $buf) ) ) {
				#$self->{_activity} = $self->{_ractivity} = AE::now;
				if ($len == $self->{read_size} and $self->{read_size} < $self->{max_read_size}) {
					$self->{read_size} *= 2;
					$self->{read_size} = $self->{max_read_size} || MAX_READ_SIZE
						if $self->{read_size} > ($self->{max_read_size} || MAX_READ_SIZE);
				}
			
				# Process buf
				my $ixx = 0;
				
				my %h;
				{do {
					if ($s{state} == 0) { # intial state, ready for request
						%h = ();
						if (( my $i = index($buf,"\012", $ixx) ) > -1) {
							if (substr($buf, $ixx, $i) =~ /(\S+) \040 (\S+) \040 HTTP\/(\d+\.\d+)/xso) {
								$s{method} = $1; $s{uri} = $2; $s{version} = $3;
							} else {
								#warn "Broken request ($i): <".substr($buf, 0, $i).">";
								return $drop->($id, "Broken request ($i): <".substr($buf, 0, $i).">");
							}
							my $ix;
							my $full;
							my $lastkey;
							while (( $i = index($buf,"\012",$ix = $i+1) ) > -1) {
								if ($i - $ix < 2) { $full = 1; last; }
								pos($buf) = $ix;
								if (substr( $buf, $ix, 1 ) =~ /[\011\040]/) { # continuation
									$buf =~ /\G[\011\040]+([^\012]+?)\015?\012/sxogc or return $drop->($id, "Bad continuation headef for $lastkey"); # or next?
									$h{ $lastkey } .= ' '.$1;
								}
								else{
									$buf =~ /\G ([^:\000-\037]+)[\011\040]*:[\011\040]*/sxogc or return $drop->($id, "Bad header line: ".substr($buf, $ix, $i-$ix));
									$lastkey = lc $1;
									$buf =~ /\G ([^\012]+?)\015?\012/sxogc or return $drop->($id);
									$h{ $lastkey } = exists $h{ $lastkey } ? $h{ $lastkey }.','.$1 : $1;
								}
							}
							$full or last;
							#warn "Full header. left: <".substr($buf,$i+1).">" if DEBUG;
							warn Dumper(\%h);
							
							if ( $s{method} eq "GET" and $s{uri} =~ m{^/favicon\.ico( \Z | \? )}sx ) {
								$write->(\$ico);
								$write->(\undef);
							} else {
								my @rv = $reqcb->( bless [ $s{method}, $s{uri}, \%h, $write ], '__HTTP__Req' );
								$s{on_body} = $rv[0] if $rv[0];
								#weaken $req;
							}
							
							
							if ($s{clen} = $h{'content-length'}) {
								if ( length($buf) - ($i + 1) >= $s{clen} ) {
									warn "Received body <".substr($buf,$i,$i+$s{clen}).">" if DEBUG;
									if ($s{on_body}) {
										$s{on_body}(substr($buf,$i,$i+$s{clen}))
									} else {
										warn "Discarding full body, no handler";
									}
									$buf = substr($buf,$i+1+$s{clen});
									$s{state} = 2;
								} else {
									warn "insufficient body, switch to state 1" if DEBUG;
									$s{state} = 1;
									if ($s{on_body}) {
										$buf = substr($buf,$i+1);
									} else {
										$buf = substr($buf,$i+1);
										#$s{clen} -= length($buf) - ( $i + 1 );
										warn "Discarding body part <$buf>. left $s{clen}";
										#$buf = '';
									}
								}
							} else {
								$s{state} = 2;
								$s{on_body}() if $s{on_body};
								$buf = substr($buf,$i+1);
							}
							
						} else {
							return; # need more
						}
					}
					if ($s{state} == 1) {
						warn "state [reading body]. need $s{clen}, have ".length($buf).": <$buf>" if DEBUG;
						if ( length $buf >= $s{clen} ) {
							warn "Received body <".substr($buf,0,$s{clen}).">" if DEBUG;
							if ($s{on_body}) {
								$s{on_body}(substr($buf,0,$s{clen}));
							} else {
								warn "Discarding remaining body, no handler";
							}
							$buf = substr($buf,$s{clen});
							#$ofs = length $buf;
							$s{state} = 2;
						} else {
							unless($s{on_body}) {
								$s{clen} -= length $buf;
								$buf = '';
							}
							return; #need more
						}
					}
					if ($s{state} == 2) {
						delete $s{on_body};
						$s{state} = 0;
					}
				} while length $buf;} # { do process buf while }
				return unless $self and exists $self->{$id};
				
				$buf = substr($buf,$ixx); # if length $buf > $ix;
			} # while read
			return unless $self and exists $self->{$id};
			$self->{$id}{rbuf} = $buf;
			if (defined $len) {
				$! = Errno::EPIPE; # warn "EOF from client ($len)";
			} else {
				return if $! == EAGAIN or $! == EINTR or $! == WSAEWOULDBLOCK;
			}
			$drop->($id, "$!");
		}; # while
	}; # rw
	
	return $self;
}

use EV;
use strict;
use Data::Dumper;


http_server 0, 8090, sub {
	warn Dumper \@_;
	my $r = shift;
	return sub {
		#warn "and body: @_";
		$r->reply(200, "I'm <b>happy to reply to $r->[1]</b> ;)", headers => {'content-type' => 'text/html'});
	};
};

EV::loop;