#!/usr/bin/env perl
use uni::perl ':dumper';
no warnings qw(internal FATAL);
use warnings qw(internal);
use Data::Dumper;
use Devel::Hexdump 'xd';
use ExtUtils::testlib;
use Devel::Leak;
use Benchmark ':all';
use EV;
use EV::HTTP::Client;
use HTTP::HeaderParser::XS;
use HTTP::Parser::XS qw(parse_http_response HEADERS_AS_HASHREF HEADERS_NONE);

my $buf = <<EOL
HTTP/1.1 200 OK
Cache-Control: no-cache,no-store,must-revalidate
Connection: close
Date: Thu, 01 Nov 2012 10:00:28 GMT
Pragma: no-cache
Server: Apache/1.3.27 (Unix) mru_xml/0.471 gorgona/2.1 mod_jk/1.2.4 mod_ruby/1.0.7 Ruby/1.6.8 mod_mrim/0.17
Content-Length: 219567
Content-Type: text/html; charset=utf-8
Expires: Wed, 02 Nov 2011 10:00:28 GMT
Last-Modified: Thu, 01 Nov 2012 14:00:28 GMT
Client-Date: Thu, 01 Nov 2012 10:00:33 GMT
Client-Peer: 94.100.180.201:80
Client-Response-Num: 1
Set-Cookie: mrcu=D8765092483C1386F12AABBFDAC3; expires=Sun, 30 Oct 2022 10:00:28 GMT; path=/; domain=.mail.ru
Multiline: some value
	continued
X-Host: lf31.mail.ru 8
X-Host: lf31.mail.ru 9

EOL
;
my $orig = $buf;
my $hdr = HTTP::HeaderParser::XS->new( \$buf );
say dumper $hdr->getHeaders();
$buf eq $orig or die "Buffer was modified";

say dumper parse_http_response($buf, HEADERS_AS_HASHREF);
$buf eq $orig or die "Buffer was modified";

say dumper EV::HTTP::Client::parse_headers($buf);
#$buf eq $orig or die "Buffer was modified";
#__END__
cmpthese timethese -.5, {
	#'hhpxs' => sub { my $hdr = HTTP::HeaderParser::XS->new( \$buf )->getHeaders() ; },
	'hpxs-h' => sub { my $hdr = parse_http_response($buf, HEADERS_AS_HASHREF);  },
	#'hpxs-n' => sub { my $hdr = parse_http_response($buf, HEADERS_NONE);  },
	'evhxs' => sub { my $hdr = EV::HTTP::Client::parse_headers($buf)  },
};


__END__

my $buf = "x"x4096;

cmpthese timethese -.5, {
	while_struct => sub { EV::HTTP::Client::while_struct($buf) },
	for_struct => sub { EV::HTTP::Client::for_struct($buf) },
	for_ptr => sub { EV::HTTP::Client::for_ptr($buf) },
	for_char => sub { EV::HTTP::Client::for_char($buf) },
	for_char_reg => sub { EV::HTTP::Client::for_char_reg($buf) },
}

