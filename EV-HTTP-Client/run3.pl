#!/usr/bin/env perl
use uni::perl ':dumper';
no warnings qw(internal FATAL);
use warnings qw(internal);
use Data::Dumper;
use Devel::Hexdump 'xd';
use ExtUtils::testlib;
use Devel::Leak;
use DDP;

use EV;
use EV::HTTP::Client;

my $run;$run = sub {
	my $x = shift;
	#Devel::Leak::NoteSV($handle);
#	$x->request( "GET / HTTP/1.0\r\nHost: google.com\r\nConnection:keep-alive\r\n\r\n", sub {
	$x->request(
		"GET /chunked/x HTTP/1.1\r\nHost: localhost\r\nConnection:keep-alive\r\n\r\n",
		sub { warn dumper $_[0]; }, # headers
		sub { warn dumper $_[0], $_[1]; }, # body
	);
	return;
	$x->request(
		"GET /chunked/x HTTP/1.1\r\nHost: localhost\r\nConnection:keep-alive\r\n\r\n",
		sub { warn dumper $_[0], $_[1]; }, # headers and glued body
	);
	$x->request(
		"GET /chunked/x HTTP/1.1\r\nHost: localhost\r\nConnection:keep-alive\r\n\r\n",
		sub { warn dumper $_[0], $_[1]; }, # headers
		sub { warn dumper $_[0], $_[1]; }, # on_body
	);
};


my $h = EV::HTTP::Client->new({
	host => '127.0.0.1',
	port => 80,
});


$run->($h);


EV::loop;
undef $h;

