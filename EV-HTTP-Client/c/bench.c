#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <endian.h>

const char const *_http11 = "HTTP/1.1";
const uint32_t const * http11 = (const uint32_t const *) "HTTP/1.1";
//const uint32_t httpx = *"HTTP/1.1";
const uint32_t HTTP = 'H' | ('T'<<8) | ( 'T' << 16 ) | ( 'P' << 24 );
const uint32_t v_11 = '/' | ('1'<<8) | ( '.' << 16 ) | ( '1' << 24 );
const uint32_t v_10 = '/' | ('1'<<8) | ( '.' << 16 ) | ( '0' << 24 );
const uint32_t v_09 = '/' | ('0'<<8) | ( '.' << 16 ) | ( '9' << 24 );

const char *x = "HTTP/0.9 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n";

typedef int (*testfunc)( char *buf );

typedef union {
	const unsigned char *c;
	const uint32_t *i;
	const uint64_t *q;
	const char *_c1;
} uniptr;

int test1(const char const *buf) {
	if( strncmp(buf, "HTTP/1.1", 8) == 0 ) {
		return 0;
	}
	return 0;
}
int test2(const char const *buf) {
	union {
		const char *c;
		const uint32_t *i;
	} p;
	p.c = buf;
	if( *p.i == *http11 ) return 0;
	return 0;
}
int test3(const char const *buf) {
	union {
		const char *c;
		const uint32_t *i;
	} p;
	p.c = buf;
	if( *p.i == *http11 ) return 0;
	return 0;
}
/*
int test3(uniptr p) {
	if( *p.i++ == HTTP ) {
		if( *p.i == v_11 ) {
			return 0;
		}
	}
	return 0;
}
*/

/*

static int test1(const char *buf) {
	//if (*buf == ' ') buf++;
	char *end;
	int i = strtoul( buf, &end, 10 );
	return i;
}

static int test2(const char const *buf) {
	uint32_t i = be32toh( *(uint32_t *)buf);
	
	int x =  ( ( i & 0xcf0000 ) >> 10 ) + ( ( i & 0xcf0000 ) >> 11 ) + ( ( i & 0xcf0000 ) >> 14 )
		+ ( ( i & 0xcf00 ) >> 5 ) + ( ( i & 0xcf00 ) >> 7 )
		+ ( i & 0xcf );
	return x;
}

static int test3(const char const *buf) {
	uint32_t i = be32toh( *(uint32_t *)buf);
	
	int x =  ( ( i & 0xcf0000 ) >> 10 ) + ( ( i & 0xcf0000 ) >> 11 ) + ( ( i & 0xcf0000 ) >> 14 )
		+ ( ( i & 0xcf00 ) >> 5 ) + ( ( i & 0xcf00 ) >> 7 )
		+ ( i & 0xcf );
	return x;
}
*/
/*

static int test1(const char *buf) {
	return strlen(buf);
}

static int test2(const char const *buf) {
	char *p = buf;
	for (;;p++) {
		if (*p ==0) return p-buf;
	}
	return 0;
}

static int test3(const char const *buf) {
	char *p = buf;
	for (;;p++) {
		if (*p ==0) return p-buf;
	}
	return 0;
}
*/



typedef struct {
	time_t      sec;
	long int    nsec;
	intmax_t    delta;
} mytimediff;

mytimediff timedelta( struct timespec t1, struct timespec t2 ) {
	mytimediff d;
	intmax_t delta_nsec = ( (intmax_t)t2.tv_nsec - (intmax_t)t1.tv_nsec );
	intmax_t delta_sec = (intmax_t)t2.tv_sec - (intmax_t)t1.tv_sec;
	if (delta_nsec < 0) { delta_nsec += 1E9; delta_sec -=1; }
	if (delta_sec < 0) { delta_nsec = 1E9 - delta_nsec; }
	intmax_t delta = delta_sec*1E9 + delta_nsec;
	
	d.sec   = delta_sec;
	d.nsec  = delta_nsec;
	d.delta = delta;
	return d;
}

long double timeit( intmax_t maxtime, testfunc test, char * data ) {
	struct timeval tv;
	struct timespec t0,t1,t2;
	
	mytimediff d;
	
	intmax_t i,k, total;
	uint64_t time1, time2;
	uint64_t tx1, tx2;
	
	//for ( i = 1; i > 0 ; i <<= 1 ) {
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t0);
	long double ops;
	for ( i = 1; i < 0xfffffff ; i <<= 1 ) {
		//printf("i=%u...",i);
		
		gettimeofday(&tv, NULL); time1 = tv.tv_sec * 1000000 + tv.tv_usec;
		clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t1);
		
		
		for ( k=0; k < i; k++) {
			test(data);
		}
		
		clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t2);
		gettimeofday(&tv, NULL); time2 = tv.tv_sec * 1000000 + tv.tv_usec;
		
		total += i;
		
		d = timedelta( t1,t2 );
		ops = i*1E9/d.delta;
		
		d = timedelta( t0,t2 );
		
		if ( d.delta > 1E8 ) {
			printf("END delta = %jd.%09jds (%jd us); (ops = %.0Lf/s)\n", d.sec, d.nsec, d.delta, ops );
			return ops;
			break;
		}
		
	}
	return -1;
}

int main(void) {
	//return;
	char *test = x;
	printf("test1 = %d\n", test1(test));
	printf("test2 = %d\n", test2(test));
	printf("test3 = %d\n", test3(test));
	
	long double ops1 = timeit( 1E10, test1, (char *) test );
	long double ops2 = timeit( 1E10, test2, (char *) test );
	long double ops3 = timeit( 1E10, test3, (char *) test );
	
	printf("ops1 = %Lf\n", ops1);
	printf("ops2 = %Lf ( %+0.2Lf%% = x%0.1Lf )\n", ops2, 100.0 * ( ops2 - ops1 ) / ops1, ops2/ops1 );
	printf("ops3 = %Lf ( %+0.2Lf%% = x%0.1Lf )\n", ops3, 100.0 * ( ops3 - ops1 ) / ops1, ops3/ops1 );
	
	return 0;
}
