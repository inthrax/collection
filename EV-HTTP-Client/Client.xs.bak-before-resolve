#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include "ppport.h"
#include "EVAPI.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <errno.h>
#include <string.h>

#include <unistd.h>
#include <stdint.h>
#include <fcntl.h>
#include <inttypes.h>
#include <sys/socket.h>
#include <netdb.h>
#include <endian.h>

#include "httpclient.h"

#ifndef likely
#define likely(x) __builtin_expect((x),1)
#define unlikely(x) __builtin_expect((x),0)
#endif

#define TAB    (char)  9
#define LF     (char) 10
#define CR     (char) 13
#define CRLF   "\x0d\x0a"


static struct addrinfo *resolve_host(char *hostname, uint16_t port) {
	int err;
	char port_str[6];
	struct addrinfo hints, *res, *res_first, *res_last;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = PF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	sprintf(port_str, "%d", port);

	err = getaddrinfo(hostname, port_str, &hints, &res_first);

	if (err) {
		croak("could not resolve hostname: %s", hostname);
		return NULL;
	}

	/* search for an ipv4 address, no ipv6 yet */
	res_last = NULL;
	for (res = res_first; res != NULL; res = res->ai_next) {
		if (res->ai_family == AF_INET)
			break;
		res_last = res;
	}

	if (!res) {
		freeaddrinfo(res_first);
		croak("could not resolve hostname: %s", hostname);
		return NULL;
	}

	if (res != res_first) {
		/* unlink from list and free rest */
		res_last->ai_next = res->ai_next;
		freeaddrinfo(res_first);
		res->ai_next = NULL;
	}

	return res;
}

#define LC_HEADER_LEN 32

typedef struct {
	size_t      len;
	char       *str;
} str_t;

typedef enum {
		start = 0,
		version,
		version_minor,
		status_sp,
		status,
		message_sp,
		message,
		cr,
		lf,
		header_name,
		header_sp,
		header_cl,
		header_cl_sp,
		header_val,
		header_cr,
		header_lf,
		header_next,
		header_last_cr,
		header_last_lf,
		skip_line
} parser_state;

typedef enum {
		hd_other = 0,
		hd_connection,
		hd_clength,
		hd_tr_encoding
} http_header;

typedef struct {
	ev_io      io;
	ev_timer   t;
	SV        *host;
	SV        *port;
	SV        *buf;
	SV        *cb;
	size_t     offset;
	enum {
		NONE = 0
	} state;
	
	struct addrinfo *saddr;
	int              s; // socket
	void      *nextcb;
	
	char      *rbuf;
	char      *ptr;
	char      *end;
	
	char      *pos;
	
	size_t     roff;
	size_t     rlen;
	
	struct {
		char  *end;
		int    v_maj;
		int    v_min;
		int    status;
		//str_t  message;
		int    content_length;
		int    chunked;
		enum {
			connection_close = 0,
			connection_keepalive,
			connection_websocket
		} connection;
	} h;
	
	struct {
		parser_state state;
		http_header  header;
		char *msg;
		char *hdr;
		int   hdrl;
		char *hdrv;
	} parse;
	
	HV * headers;
	
} HttpRequest;

typedef void (*http_cb)( HttpRequest *, ... );

static void on_timer( EV_P_ ev_timer *w, int revents ) {
	HttpRequest *r = (HttpRequest *) (((char *)w) - offsetof (HttpRequest, t));
	
}

static void on_connected( EV_P_ ev_io *w, int revents ) {
	//HttpRequest *r = (HttpRequest *) (((char *)w) - offsetof (HttpRequest, io));
	HttpRequest *r = (HttpRequest *) w;
	
}

typedef void (*http_resolve_cb)( HttpRequest *, in_addr_t addr );

static void http_resolve( HttpRequest * r, http_resolve_cb cb ) {
	struct in_addr ip;
	struct addrinfo hints, *res, *res_first, *res_last;
	if ( inet_aton( SvPV_nolen(r->host), &ip ) ) {
		
		warn("inet_aton ok");
		res = safemalloc( sizeof( struct addrinfo ) );
		memset(res, 0, sizeof( struct addrinfo ));
		res->ai_addr = safemalloc( sizeof( struct sockaddr_in ) );
		memset(res->ai_addr, 0, sizeof( struct sockaddr_in ));
		
		res->ai_family   = AF_INET;
		res->ai_socktype = SOCK_STREAM;
		res->ai_protocol = IPPROTO_TCP;
		res->ai_addrlen  = sizeof( struct sockaddr_in );
		memcpy( &( (( struct sockaddr_in *)(res->ai_addr))->sin_addr ), &ip, sizeof( struct in_addr ) );
		(( struct sockaddr_in *)(res->ai_addr))->sin_port = htons( SvUV( r->port ) );
		
		//warn ("resolved: %p (%s)", res, inet_ntoa( (( struct sockaddr_in *)(res->ai_addr))->sin_addr ));
		
		return cb(r, res);
		
		//r->saddr = res;
		//	r->s = socket(r->saddr->ai_family, r->saddr->ai_socktype, r->saddr->ai_protocol);
	} else {
		warn("inet_aton failed for %s", SvPV_nolen(r->host));
	}
	
	int err;
	memset(&hints, 0, sizeof(hints));
	
	//hints.ai_family   = PF_UNSPEC;
	hints.ai_family   = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	
	
	err = getaddrinfo(SvPV_nolen(r->host), SvPV_nolen( r->port ), &hints, &res_first);

	if (err) {
		warn ("could not resolve hostname: %s", SvPVX(r->host));
		return cb(r, NULL);
	}

	/* search for an ipv4 address, no ipv6 yet */
	res_last = NULL;
	for (res = res_first; res != NULL; res = res->ai_next) {
		//warn("address: %p (%p -> %s)", res, res->ai_addr, res->ai_addr ? inet_ntoa( (( struct sockaddr_in *)(res->ai_addr))->sin_addr ) : NULL );
		if (res->ai_family == AF_INET)
			break;
		res_last = res;
	}

	if (!res) {
		freeaddrinfo(res_first);
		warn("could not resolve hostname: %s", SvPVX(r->host));
		return cb(r, NULL);
	}

	if (res != res_first) {
		/* unlink from list and free rest */
		res_last->ai_next = res->ai_next;
		freeaddrinfo(res_first);
		res->ai_next = NULL;
	}
	return cb(r, res);
}

static void call_back( HttpRequest *r, int args, ...) {
	dSP;
	int i;
	va_list ap;
	va_start(ap, args);
	
	PUSHMARK(SP);
	EXTEND( SP, args );
	for (i=0; i < args; i++) {
		PUSHs( va_arg(ap, SV *) );
	}
	va_end(ap);
	PUTBACK;
	
	call_sv( r->cb, G_DISCARD | G_VOID | G_EVAL | G_KEEPERR );
}

typedef void (*http_connect_cb)( HttpRequest *, struct sockaddr_in *peer );

static void http_connect( HttpRequest * r, http_connect_cb cb );
static void on_connect ( HttpRequest *r, struct sockaddr_in *peer );

static void on_resolve ( HttpRequest *r, struct addrinfo *res ) {
	// struct sockaddr: res->ai_addr
	struct sockaddr_in *iaddr = ( struct sockaddr_in *)res->ai_addr;
	
	warn ("resolved: %p (%s:%d)", res, inet_ntoa( iaddr->sin_addr ), ntohs( iaddr->sin_port ));
	r->saddr = res;
	do {
		r->s = socket(r->saddr->ai_family, r->saddr->ai_socktype, r->saddr->ai_protocol);
	} while (r->s == -1 && errno == EINTR);
	
	if (r->s == -1) {
		return call_back(r, 0);
	}
	fcntl(r->s, F_SETFL, O_NONBLOCK | O_RDWR);
	
	http_connect(r, on_connect);
}

static void on_connect_io( struct ev_loop *loop, ev_io *w, int revents ) {
	HttpRequest *r = (HttpRequest *) w;
	warn("on_connect_io ");
	struct sockaddr_in peer;
	socklen_t addrlen = sizeof(peer);
	again:
	if( getpeername( r->s, ( struct sockaddr *)&peer, &addrlen) == 0 ) {
		warn("connected address: %s:%d",  inet_ntoa( peer.sin_addr ), ntohs( peer.sin_port ) );
		ev_io_stop( loop, w );
	} else {
		warn("errno: %s",strerror(errno));
		switch( errno ) {
			case EINTR:
				goto again;
			case EAGAIN:
				return;
			case ENOTCONN: {
				char x[1];
				recv( r->s, x,1,0 );
				warn("errno: %s",strerror(errno));
				ev_io_stop( loop, w );
				return;
			}
			default:
				return call_back(r, 0);
		}
	}
	
	warn("connect success");
	http_connect_cb cb = (http_connect_cb) r->nextcb;
	cb(r, &peer);
}


static void http_connect111( HttpRequest * r, http_connect_cb cb ) {
/*	
	struct sockaddr_in *iaddr = ( struct sockaddr_in *) r->saddr->ai_addr;
	//memcpy( &( (( struct sockaddr_in *)(res->ai_addr))->sin_addr ), &ip, sizeof( struct in_addr ) );
	warn("making connect to %s:%d", inet_ntoa( iaddr->sin_addr ), ntohs( iaddr->sin_port ) );
	
	r->nextcb = (void *) cb;
	
	start:
	if (connect( r->s, r->saddr->ai_addr, r->saddr->ai_addrlen) != -1
	//||  errno == EISCONN
	) {
		warn("connected");
		return on_connect_io(EV_DEFAULT, &r->io, 0);
		//return cb(r);
	} else {
		warn("connect:%s...",strerror(errno));
		switch (errno) {
			case EINPROGRESS:
			case EALREADY:
			case EWOULDBLOCK:
				// async connect now in progress
				//client->state = CLIENT_CONNECTING;
				warn( "in progress" );
				break;
			case EINTR:
				goto start;
			default: {
				return call_back(r,2,NULL,newSVpv(strerror(errno),0));
				//strerror_r(errno, client->buffer, sizeof(client->buffer));
				//W_ERROR("connect() failed: %s (%d)", client->buffer, errno);
				//return 0;
			}
		}
	}
	
	
	warn( "make io");
	ev_io_init( &r->io, on_connect_io, r->s, EV_WRITE );
	warn( "make io: init ok");
	ev_io_start( EV_DEFAULT, &r->io );
	warn( "make io: start ok");
	*/
}

static void http_connect( HttpRequest * r, http_connect_cb cb ) {
	
	struct sockaddr_in *iaddr = ( struct sockaddr_in *) r->saddr->ai_addr;
	warn("making connect to %s:%d", inet_ntoa( iaddr->sin_addr ), ntohs( iaddr->sin_port ) );
	
	r->nextcb = (void *) cb;
	
	start:
	
	if ( connect( r->s, r->saddr->ai_addr, r->saddr->ai_addrlen) == 0 ) {
		warn("connected ?");
		//return on_connect_io(EV_DEFAULT, &r->io, 0);
		//return;
	} else {
		warn("connect:%s...",strerror(errno));
		switch (errno) {
			case EINPROGRESS:
			case EALREADY:
			case EWOULDBLOCK:
				// async connect now in progress
				//client->state = CLIENT_CONNECTING;
				warn( "in progress" );
				break;
			case EINTR:
				goto start;
			default: {
				return call_back(r,2,NULL,newSVpv(strerror(errno),0));
				//strerror_r(errno, client->buffer, sizeof(client->buffer));
				//W_ERROR("connect() failed: %s (%d)", client->buffer, errno);
				//return 0;
			}
		}
	}
	
	
	warn( "make io");
	ev_io_init( &r->io, on_connect_io, r->s, EV_WRITE );
	warn( "make io: init ok");
	ev_io_start( EV_DEFAULT, &r->io );
	warn( "make io: start ok");
}

// TODO: check _
static char  lowcase[] =
	"\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"
	"\0\0\0\0\0\0\0\0\0\0\0\0\0-\0\0" "0123456789\0\0\0\0\0\0"
	"\0abcdefghijklmnopqrstuvwxyz\0\0\0\0_"
	"\0abcdefghijklmnopqrstuvwxyz\0\0\0\0\0"
	"\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"
	"\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"
	"\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"
	"\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";

static int parse_http ( HttpRequest *r ) {

	register parser_state  state; // state of parsing
	register char *p = r->pos;    // main pointer
	register char *e = r->ptr;    // end of buffer
	register char  c;             // current char
	
	// restove values
	state      = r->parse.state;
	char *msg  = r->parse.msg;
	char *hdr  = r->parse.hdr;
	int   hdrl = r->parse.hdrl;
	char *hdrv = r->parse.hdrv;

	
	int i;
	
	char err[256];err[0] = 0;
	
#define myerror(msg, ...) STMT_START {\
	snprintf(err,256,msg " at %s line %d\n", ##__VA_ARGS__, __FILE__, __LINE__); \
	goto error; \
} STMT_END
	
	
	//while (r->pos < r->ptr) {
	for (; p < e; p++) {
		c = *p;
		//warn("state=%d, pos='%c'", state, *(r->pos));
		switch(state) {
			case start:
				if (unlikely( e - p < 5 )) goto shortread;
				if (strncmp(p, "HTTP/", 5) == 0) {
					p += 4;
					i = 0;
					msg = p+1;
					state = version;
					break;
				} else {
					snprintf(err,256,"Expected 'HTTP/', received '%-.5s'...", p);
					goto error;
				}
				//break;
			case version:
				//warn("read ver from >%-.10s...", p);
				if ( (c >= '0' && c <= '9' ) ) {
					i = i * 10 + c - '0';
					break;
				}
				else
				if ( c == '.' ) {
					r->h.v_maj = i;
					i=0;
					state = version_minor;
					break;
				}
				else {
					myerror("Bad version: ..%-.5s...", p);
				}
				
				state = status_sp;
				//break;
			case version_minor:
				//warn("read ver min from >%-.10s...", p);
				if ( (c >= '0' && c <= '9' ) ) {
					i = i * 10 + c - '0';
					break;
				}
				else
				if (c != ' ' && c != TAB) {
					// skip?
					break;
				}
				r->h.v_min = i;
				state = status_sp;
				
				(void) hv_stores( r->headers, "HTTPVersion", newSVpvn( msg, p - msg ));
				//warn("read ver: %d.%d", r->h.v_maj, r->h.v_min);
				//break;
			case status_sp:
				if (c == ' ' || c == TAB) {
					break;
				}
				state = status;
				//break;
			case status:
				//warn("read status from >%-.10s...", p);
				if ( (c >= '0' && c <= '9' )) {
					r->h.status = r->h.status * 10 + c - '0';
					break;
				}
				(void) hv_stores( r->headers, "Status", newSViv( r->h.status ));
				//warn("read status: %d", r->h.status);
				state = message_sp;
				//break;
			case message_sp:
				if (c == ' ' || c == TAB) {
					break;
				}
				msg = p;
				state = message;
				//break;
			case message:
				//warn("read message from >%-.10s...", p);
				if ( (c != CR && c != LF ) ) {
					break;
				}
				//state = status_sp;
				
				hv_stores( r->headers, "Reason", newSVpvn( msg, p - msg ));
				//warn("read msg: '%-.*s'", r->h.message.len, r->h.message.str);
				state = cr;
				//break;
			case cr:
				if ( c == CR ) {
					c = *++p;
				}
				state = lf;
				if ( p == e ) goto shortread;
				//break;
			case lf:
				if ( c == LF ) {
					state = header_next;
					break;
				} else {
					myerror("Expected LF, received %02x", c);
					//snprintf(err,256,"Expected LF, received %02x", c);
					//goto error;
				}
				//break;
			case header_next:
				if ( unlikely( c == '\t' || c == ' ' ) ) {
					if (hdr) {
						state = header_val;
						warn ("header continuation");
					}
					// else just skip wsp
					break;
				}
				else
				if ( unlikely( c == '\r' || c == '\n' ) ) {
					state = header_last_cr;
					break;
				}
				else {
					state = header_name;
					hdr = p;
					hdrv = 0;
					if( lowcase[c] ) {
						*p = lowcase[c];
						break;
					}
					else
					if (c == 0) {
						myerror("Invalid header, \\0 encountered");
					}
					else {
						state = skip_line;
						break;
					}
				}
			case header_name:
				//if (!hdr) hdr = p;
				if (lowcase[c]) {
					*p = lowcase[c];
					//r->lch[i++] = lc;
					//i &= (LC_HEADER_LEN - 1);
					break;
				}
				
				
				//r->lch[  i & (LC_HEADER_LEN - 1) ] = 0;
				hdrl = p - hdr;
				
				if (strncmp( hdr, "content-length", hdrl ) == 0) {
					r->parse.header = hd_clength;
					i = 0;
				}
				else
				if (strncmp( hdr, "connection", hdrl ) == 0) {
					r->parse.header = hd_connection;
					i = 0;
				}
				else
				if (strncmp( hdr, "transfer-encoding", hdrl ) == 0) {
					r->parse.header = hd_tr_encoding;
					i = 0;
				}
				else
				{
					//warn("compare %s failed", r->lch);
					r->parse.header = hd_other;
				}
				
				switch(c) {
					case ':':
						state = header_cl_sp;
						break;
					case ' ':
					case TAB:
						state = header_sp;
						break;
					case CR:
						hv_store( r->headers, hdr, hdrl, &PL_sv_undef, 0 );
						//hdrvl = 0; // value is 0
						state = skip_line;
						break;
					case LF:
						state = header_next;
						break;
					
				}
				break;
				//if (c != ' ' && c != TAB && c != ':') {
				//	break;
				//}
				
			case header_sp:
				if (c == ' ' || c == TAB) {
					break;
				}
				state = header_cl;
			case header_cl:
				if(c == ':') {
					state = header_cl_sp;
					break;
				} else {
					myerror("Expected ':', received '%c' (%02x)", c, c);
					goto error;
				}
				state = header_cl;
			case header_cl_sp:
				if(c == ' ' || c == TAB) {
					break;
				}
				state = header_val;
				hdrv = p;
			case header_val:
				if (c != CR && c != LF ) {
					break;
				}
				
				switch(r->parse.header) {
					case hd_clength:
						i = 0;
						;
						for ( msg = hdrv ; msg < p; msg++ ) {
							if ( (*msg >= '0' && *msg <= '9' )) {
								i = i * 10 + *msg - '0';
							}
							else
							if ( *msg == ' ' || *msg == TAB ) {
								//skip if
							}
							else {
								myerror("Expected digits in content-length field, received: %02x",*msg);
							}
						}
						r->h.content_length = i;
						warn("Content-Length = %d", i);
						break;
					case hd_tr_encoding:
						if (strncasecmp( hdrv, "chunked", p - hdrv ) == 0) {
							r->h.chunked = 1;
							warn("Transfer-Encoding=chunked", i);
						}
						else {
							warn("Unknown transfer-encoding: %-.*s", p - hdrv, hdrv );
						}
						break;
					case hd_connection:
						
						if (strncasecmp( hdrv, "keep-alive", p - hdrv ) == 0) {
							r->h.connection = connection_keepalive;
							warn("Connection = KA", i);
						}
						else
						if (strncasecmp( hdrv, "close", p - hdrv ) == 0) {
							r->h.connection = connection_close;
						}
						else {
							r->h.connection = connection_close;
							warn("Unknown transfer-encoding: %-.*s", p - hdrv, hdrv );
						}
						
				}
				
				hv_store( r->headers, hdr, hdrl, newSVpvn( hdrv, p - hdrv ), 0 );
				
				state = header_cr;
			case header_cr:
				if ( c == '\r' ) {
					c = *++p;
				}
				state = lf;
				if ( p == e ) goto shortread;
				//break;
			case header_lf:
				if ( c == '\n' ) {
					state = header_next;
					break;
				} else {
					myerror("Expected LF, received %02x", c);
				}
			case header_last_cr:
				if ( c == '\r' ) {
					c = *++p;
				}
				state = header_last_lf;
				if ( p == e ) goto shortread;
			case header_last_lf:
				if ( c == '\n' ) {
					p++;
					goto last;
				} else {
					snprintf(err,256,"Expected LF, received %02x", c);
					goto error;
				}
			case skip_line:
				switch(c) {
					case LF:
						state = header_next;
						break;
					default:
						break;
				}
				break;
			default:
				myerror("Unhandled state: %d", state);
		}
	}
	
	shortread:
		warn("not enough data");
		r->pos = p;
		r->parse.state = state;
		r->parse.msg  = msg;
		r->parse.hdr  = hdr;
		r->parse.hdrv = hdrv;
		return -1;
	
	last:
		warn("done. Body start: >%-.10s", p);
		r->h.end = p;
		r->pos = p;
		
		return 0;
	error:
		warn("error: %s", err);
		return 1;
}

static void on_read_ready( struct ev_loop *loop, ev_io *w, int revents ) {
	HttpRequest *r = (HttpRequest *) w;
	warn("read ready");
	size_t rd;
	//rd = recv( r->s, r->ptr, 10, 0 );
	rd = recv( r->s, r->ptr, r->end - r->ptr, 0 );
	r->ptr += rd;
	*( r->ptr + 1 ) = 0;
	r->roff += rd;
	//warn("read: %d ",rd);
	
	//r->parse.state = start;
	
	if (!r->h.end) {
		// not parsed headers yet.
		
		int result = parse_http( r );
		
		 // not enough data
		if (result < 0) return;
		
		if (result != 0) {
			// error
			ev_io_stop( loop, w );
			return;
		}
	}
	
		if (r->h.content_length > 0) {
			warn("Have clength %d and have %d data", r->h.content_length, r->ptr - r->h.end);
			if ( r->h.content_length <= r->ptr - r->h.end ) {
				warn("Have enough content");
				ev_io_stop( loop, w );
				
				errno = 0;
				return call_back(r, 2, newRV_noinc((SV *) r->headers), newSVpvn( r->h.end, r->h.content_length ));
				
				if( r->h.content_length < r->ptr - r->h.end ) {
					warn("Have trailing: [%d] %s",(r->ptr - r->h.end) - r->h.content_length, r->h.end + r->h.content_length);
				}
			} else {
				warn("Have not enough content");
			}
		}
		
}

static void on_write_ready( struct ev_loop *loop, ev_io *w, int revents ) {
	HttpRequest *r = (HttpRequest *) w;
	STRLEN len;
	
	char *buf = SvPV( r->buf, len );
	buf += r->offset;
	len -= r->offset;
	
	warn("sending [%d] %s",len,buf);
	size_t wr;
	wr = send( r->s, buf, len, 0 );
	warn ("written %d of %d", wr, len);
	if ( wr == len ) {
		// success
		warn("Written all");
		ev_io_stop( loop, w );
		
		ev_io_init( &r->io, on_read_ready, r->s, EV_READ );
		ev_io_start( loop, &r->io );
		
	} else {
		r->offset += wr;
	}
}

static void on_connect ( HttpRequest *r, struct sockaddr_in *peer ) {
	// struct sockaddr: res->ai_addr
	warn("connected : %s:%d",  inet_ntoa( peer->sin_addr ), peer->sin_port );
	STRLEN len;
	
	char *buf = SvPV( r->buf, len );
	warn("sending [%d] %s",len,buf);
	size_t wr;
	wr = send( r->s, buf, len -5, 0 );
	warn ("written %d of %d", wr, len);
	if ( wr == len ) {
		// success
		warn("Written all");
		
		ev_io_init( &r->io, on_read_ready, r->s, EV_READ );
		ev_io_start( EV_DEFAULT, &r->io );
		
	} else {
		r->offset += wr;
		ev_io_init( &r->io, on_write_ready, r->s, EV_WRITE );
		ev_io_start( EV_DEFAULT, &r->io );
		return;
	}
}


MODULE = EV::HTTP::Client		PACKAGE = EV::HTTP::Client

BOOT:
{
    I_EV_API ("EV::HTTP::Client");
}

SV *
new(SV *pk, HV * conf)
CODE:
	HV *stash = gv_stashpv(SvPV_nolen(pk), TRUE);
	HttpRequest * r = safemalloc( sizeof(HttpRequest) );
	if (!r) croak("Can't allocate request");
	memset(r,0,sizeof(HttpRequest));
	r->rbuf = safemalloc( r->rlen = 128 * 1024 );
	if (!r->rbuf) {
		safefree(r);
		croak("Can't allocate buffer");
	}
	r->pos = r->ptr = r->rbuf;
	r->end = r->rbuf + r->rlen;
	r->headers = newHV();
	

	SV **key;
	if ((key = hv_fetchs(conf, "host", 0)) && SvOK(*key)) {
		r->host = *key;
		SvREFCNT_inc(r->host);
	}
	if ((key = hv_fetchs(conf, "port", 0)) && SvOK(*key)) {
		r->port = *key;
		SvREFCNT_inc(r->port);
	} else {
		r->port = newSViv(80);
		SvREFCNT_inc(r->port);
	}
	
	r->state = NONE;
	ST(0) = sv_2mortal (sv_bless (newRV_noinc (newSViv(PTR2IV( r ))), stash));
	XSRETURN(1);

void
DESTROY(SV *self)
CODE:
	HttpRequest * r = ( HttpRequest * ) SvUV( SvRV( self ) );
	if (r->host)     { SvREFCNT_dec(r->host); }
	if (r->port)     { SvREFCNT_dec(r->port); }
	if (r->cb)       { SvREFCNT_dec(r->cb); }
	if( r->headers ) { SvREFCNT_dec(r->headers); }
	safefree(r->rbuf);
	safefree(r);


void
request(SV *self, SV *svbuf, SV *cb)
PROTOTYPE: $$$
CODE:
	HttpRequest * r = ( HttpRequest * ) SvUV( SvRV( self ) );
	warn("Performing request to %s:%d\n%s\n", SvPV_nolen(r->host), SvIV( r->port ), SvPV_nolen(svbuf));
	
	/*
		resolve -> connect -> write -> read
		
	*/
	
	//r->cb = cb;
	SvREFCNT_inc(r->cb = cb);
	SvREFCNT_inc(r->buf = svbuf);
	
	http_resolve(r, on_resolve);
	
	
	XSRETURN_UNDEF;



void
http_request(host, port, req, ...)
	char *host;
	U16   port;
	char *req;
	PROTOTYPE: $$$@
	CODE:
		warn("call %s:%d -> %s", host,port,req);
		
		/*
		Config config;
		memset(&config, 0, sizeof(Config));
		Worker worker;
		memset(&worker, 0, sizeof(Worker));
		worker.config = &config;
		worker.loop = ev_default_loop(0);

		config.concur_count = 1;
		config.req_count = 10;
		config.saddr = saddr;
		
		Client *client = client_new( &worker );
		
		client_state_machine(client);
		*/
		
		//connect, send request, read and parse response
		
		//url_t url;

		//parse_url(&url, urlc);
		//warn("call sc<%s> host<%s> port<%d>", url.scheme, url.host, url.port, url.path, url.query, url.fragment, url.username, url.password);
		warn("returning");
		
		XSRETURN_UNDEF;


void
while_struct(SV * buf)
CODE:
	char c;
	HttpRequest xr, *r;
	r = &xr;
	r->pos = SvPV( buf, r->rlen );
	r->end = r->pos + r->rlen;
	while( r->pos < r->end ) {
		r->pos++;
		c = *r->pos;
		switch(c) {
			case 1:
				break;
			case 2:
				break;
			case 3:
				break;
			case 4:
				break;
			default:
				break;
		}
	}
	XSRETURN_UNDEF;

void
for_struct(SV * buf)
CODE:
	char c;
	HttpRequest xr, *r;
	r = &xr;
	r->pos = SvPV( buf, r->rlen );
	r->end = r->pos + r->rlen;
	for(; r->pos < r->end; r->pos++) {
		c = *r->pos;
		switch(*r->pos) {
			case 1:
				break;
			case 2:
				break;
			case 3:
				break;
			case 4:
				break;
			default:
				break;
		}
	}
	XSRETURN_UNDEF;

void
for_ptr(SV * buf)
CODE:
	char c;
	HttpRequest xr, *r;
	r = &xr;
	r->pos = SvPV( buf, r->rlen );
	r->end = r->pos + r->rlen;
	char *p, *e = r->end;
	for(p = r->pos; p < e; p++) {
		switch(*p) {
			case 1:
				break;
			case 2:
				break;
			case 3:
				break;
			case 4:
				break;
			default:
				break;
		}
	}
	r->pos = p;
	XSRETURN_UNDEF;

void
for_char(SV * buf)
CODE:
	char c;
	HttpRequest xr, *r;
	r = &xr;
	r->pos = SvPV( buf, r->rlen );
	r->end = r->pos + r->rlen;
	char *p, *e = r->end;
	for(p = r->pos; p < e; p++) {
		c = *p;
		switch(c) {
			case 1:
				break;
			case 2:
				break;
			case 3:
				break;
			case 4:
				break;
			default:
				break;
		}
	}
	r->pos = p;
	XSRETURN_UNDEF;

void
for_char_reg(SV * buf)
CODE:
	register char  c;
	register char *p;
	register char *e;
	HttpRequest xr, *r;
	r = &xr;
	r->pos = SvPV( buf, r->rlen );
	r->end = r->pos + r->rlen;
	e = r->end;
	for(p = r->pos; p < e; p++) {
		c = *p;
		switch(c) {
			case 1:
				break;
			case 2:
				break;
			case 3:
				break;
			case 4:
				break;
			default:
				break;
		}
	}
	r->pos = p;
	XSRETURN_UNDEF;
