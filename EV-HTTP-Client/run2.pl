#!/usr/bin/env perl
use uni::perl ':dumper';
no warnings qw(internal FATAL);
use warnings qw(internal);
use Data::Dumper;
use Devel::Hexdump 'xd';
use ExtUtils::testlib;
use Devel::Leak;
use DDP;

use EV;
use EV::HTTP::Client;

my $t;$t = EV::timer 0,1,sub {
	say ".";
	$t;
};


my $count = 2;
my $handle;

=for rem

$x->req(sub {
	my $r;
	$r->headers;
	
	$r->body(sub {
	
	});
})

=cut

my $run;$run = sub {
	my $x = shift;
	#Devel::Leak::NoteSV($handle);
#	$x->request( "GET / HTTP/1.0\r\nHost: google.com\r\nConnection:keep-alive\r\n\r\n", sub {
	$x->request( "GET /chunked/x HTTP/1.1\r\nHost: localhost\r\nConnection:keep-alive\r\n\r\n", sub {
		warn dumper \@_, $!;
		#return EV::unloop;
		if ($count-- > 0) {
			$run->($x);
		} else {
			EV::unloop();
		}
	} );
};


#=for rem
my $h = EV::HTTP::Client->new({
	host => '127.0.0.1',
	port => 80,
});


$run->($h);
#=cut


=for rem
EV::HTTP::Client::http_request
	'google.com', 80,
	"GET / HTTP/1.0\r\nHost: google.com\r\n\r\n",
	sub {
		warn "result: @_";
		EV::unloop;
	};
=cut

EV::loop;
undef $h;

#Devel::Leak::CheckSV($handle);
