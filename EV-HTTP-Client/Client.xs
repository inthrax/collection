#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#define NEED_newRV_noinc
#define NEED_newSVpvn_share
#define NEED_sv_2pv_flags

#include "ppport.h"
#include "EVAPI.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <errno.h>
#include <string.h>

#include <unistd.h>
#include <stdint.h>
#include <fcntl.h>
#include <inttypes.h>
#include <sys/socket.h>
#include <netdb.h>
#include <endian.h>

#include "httpfast.h"

#ifndef likely
#define likely(x) __builtin_expect((x),1)
#define unlikely(x) __builtin_expect((x),0)
#endif

#define TAB    (char)  9
#define LF     (char) 10
#define CR     (char) 13
#define CRLF   "\x0d\x0a"

#define UNDEF  &PL_sv_undef

//	fprintf(stderr, WHERESTR, WHEREARG);
#define MYDEBUG
#ifdef MYDEBUG
#define WHERESTR    " at %s line %d.\n"
#define WHEREARG    __FILE__, __LINE__
#define debug(fmt, ...)   do{ \
	fprintf(stderr, "[DEBG] %s:%d: ", __FILE__, __LINE__); \
	fprintf(stderr, fmt, ##__VA_ARGS__); \
	if (fmt[strlen(fmt) - 1] != CR) { fprintf(stderr, "\n"); } \
	} while(0)
#else
#define debug(...)
#endif

#define cwarn(fmt, ...)   do{ \
	fprintf(stderr, "[WARN] %s:%d: ", __FILE__, __LINE__); \
	fprintf(stderr, fmt, ##__VA_ARGS__); \
	if (fmt[strlen(fmt) - 1] != CR) { fprintf(stderr, "\n"); } \
	} while(0)


#define str2toeol( x ) ( strchr( x, LF ) ? ( (int) ( strchr( x, LF ) - x ) ) : (int) strlen(x) ), x

struct _HttpRequest;
typedef struct _HttpRequest HttpRequest;
typedef void (*http_cb)( HttpRequest *, unsigned int, ... );

typedef enum {
		NONE = 0,
		PARSE_LINE,
		PARSE_HEADERS,
		RECV_BODY

} HttpRequestState;

struct _HttpRequest {
	ev_io      io;
	ev_timer   t;
	SV        *host;
	SV        *port;
	
	// Current request
	SV        *wbuf;
	SV        *cb;     // headers or generic callback
	SV        *cbb;    // body callback
	
	//http_cb    ccb;    // c    callback
	
	// Pending requests
	AV * requests;
	
	HttpRequestState state;
	
	parse_http_state http_state;
	
	struct in_addr  iaddr;
	int              s; // socket
	void      *nextcb;
	
	STRLEN     woff; //write offset
	
	char      *rbuf;
	char      *ptr;
	char      *end;
	
	char      *pos;
	
	int        count; // simple counted
	
} ;


static char  lowcase[] =
	"\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"
	"\0\0\0\0\0\0\0\0\0\0\0\0\0-\0\0" "0123456789\0\0\0\0\0\0"
	"\0abcdefghijklmnopqrstuvwxyz\0\0\0\0_"
	"\0abcdefghijklmnopqrstuvwxyz\0\0\0\0\0"
	"\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"
	"\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"
	"\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"
	"\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";

/*
#define call_back(req, argn, ...) STMT_START { \
	if ( r->cb ) { \
		call_back_cv(req, argn, ##__VA_ARGS__);\
	} else if (r->ccb) {\
		r->ccb(req, (unsigned int) argn, ##__VA_ARGS__); \
	} else { \
		cwarn("Neither cb nor ccb set"); \
	} \
} STMT_END
*/
/*
#define call_back(req, argn, ...) STMT_START { \
	if ( r->cb ) { \
		call_back_cv(req, argn, ##__VA_ARGS__);\
	} else { \
		cwarn("Neither cb nor ccb set"); \
	} \
} STMT_END
*/
#define call_back(req, argn, ...) STMT_START { \
	cwarn("FIXME!");\
} STMT_END

static void call_back_cv( SV * cv, int args, ...) {
	dSP;
	int i;
	va_list ap;
	va_start(ap, args);
	
	//if
	ENTER;
	SAVETMPS;
	//end

	
	PUSHMARK(SP);
	EXTEND( SP, args );
	
	for (i=0; i < args; i++) {
		SV * arg = va_arg(ap, SV *);
		if (arg && SvOK( arg )) {
			debug("push arg: %p (%d) %s", arg, arg ? SvTYPE(arg) : -1, arg ? SvPV_nolen(arg) : "NULL");
			XPUSHs( sv_2mortal( arg ) );
		}
		else {
			debug("skip arg: %p (%d) %s", arg, arg ? SvTYPE(arg) : -1, arg ? SvPV_nolen(arg) : "NULL");
			XPUSHs( UNDEF );
		}
	}
	va_end(ap);
	PUTBACK;
	debug("call %p", cv);
	call_sv( cv, G_DISCARD | G_VOID );
	
	//call_sv( cv, G_DISCARD | G_VOID | G_EVAL | G_KEEPERR );
	
	//SvREFCNT_dec( r->cb );
	//r->cb = 0;
	
	//if
	FREETMPS;
	LEAVE;
	//end
	
}

static void call_errno( HttpRequest *r, int err) {
	return call_back(r,2, NULL, newSVpv(strerror(err),0) );
}

static void call_error( HttpRequest *r, SV * err) {
	return call_back(r,2, NULL, err );
}



typedef void (*http_resolve_cb)( HttpRequest *, struct in_addr );
typedef void (*http_connect_cb)( HttpRequest *, struct sockaddr_in *peer );


static void http_resolve( HttpRequest *, http_resolve_cb );
static void on_resolve ( HttpRequest *, struct in_addr );
static void http_connect( HttpRequest *, http_connect_cb );
static void on_connect ( HttpRequest *, struct sockaddr_in * );
static void on_connect_io( struct ev_loop *, ev_io *, int );
static void on_write_ready( struct ev_loop *loop, ev_io *w, int revents );
static void on_read_ready( struct ev_loop *loop, ev_io *w, int revents );


/*

static void on_timer( EV_P_ ev_timer *w, int revents ) {
	HttpRequest *r = (HttpRequest *) (((char *)w) - offsetof (HttpRequest, t));
	warn("on_timer: %p",r);
}

static void on_connected( EV_P_ ev_io *w, int revents ) {
	//HttpRequest *r = (HttpRequest *) (((char *)w) - offsetof (HttpRequest, io));
	HttpRequest *r = (HttpRequest *) w;
	warn("on_cnn: %p",r);
	
}

*/


static void http_resolve( HttpRequest * r, http_resolve_cb cb ) {
	struct in_addr ip;
	struct addrinfo hints, *res, *res_first, *res_last;
	if ( inet_aton( SvPV_nolen(r->host), &ip ) ) {
		//warn("inet_aton ok: %s", inet_ntoa( ip ));
		return cb(r, ip);
		/*
		warn("inet_aton ok");
		res = safemalloc( sizeof( struct addrinfo ) );
		memset(res, 0, sizeof( struct addrinfo ));
		res->ai_addr = safemalloc( sizeof( struct sockaddr_in ) );
		memset(res->ai_addr, 0, sizeof( struct sockaddr_in ));
		
		res->ai_family   = AF_INET;
		res->ai_socktype = SOCK_STREAM;
		res->ai_protocol = IPPROTO_TCP;
		res->ai_addrlen  = sizeof( struct sockaddr_in );
		memcpy( &( (( struct sockaddr_in *)(res->ai_addr))->sin_addr ), &ip, sizeof( struct in_addr ) );
		(( struct sockaddr_in *)(res->ai_addr))->sin_port = htons( SvUV( r->port ) );
		
		//warn ("resolved: %p (%s)", res, inet_ntoa( (( struct sockaddr_in *)(res->ai_addr))->sin_addr ));
		
		
		
		//r->saddr = res;
		//	r->s = socket(r->saddr->ai_family, r->saddr->ai_socktype, r->saddr->ai_protocol);
		*/
	} else {
		//warn("inet_aton failed for %s", SvPV_nolen(r->host));
	}
	
	int err;
	memset(&hints, 0, sizeof(hints));
	
	//hints.ai_family   = PF_UNSPEC;
	hints.ai_family   = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	
	
	err = getaddrinfo(SvPV_nolen(r->host), SvPV_nolen( r->port ), &hints, &res_first);

	if (err) {
		return call_error(r, newSVpvf("could not resolve hostname '%s'", SvPVX(r->host)));
	}

	/* search for an ipv4 address, no ipv6 yet */
	res_last = NULL;
	for (res = res_first; res != NULL; res = res->ai_next) {
		//warn("address: %p (%p -> %s)", res, res->ai_addr, res->ai_addr ? inet_ntoa( (( struct sockaddr_in *)(res->ai_addr))->sin_addr ) : NULL );
		if (res->ai_family == AF_INET)
			break;
		res_last = res;
	}

	if (!res) {
		freeaddrinfo(res_first);
		return call_error(r, newSVpvf("could not resolve hostname '%s'", SvPVX(r->host)));
	}

	if (res != res_first) {
		/* unlink from list and free rest */
		res_last->ai_next = res->ai_next;
		freeaddrinfo(res_first);
		res->ai_next = NULL;
	}
	
	return cb(r, (( struct sockaddr_in *)(res->ai_addr))->sin_addr);
}

static void on_resolve ( HttpRequest *r, struct in_addr iaddr ) {
	r->iaddr = iaddr;
	//warn ("resolved: (%s)", inet_ntoa( r->iaddr ));
	http_connect(r, on_connect);
}

static void http_connect( HttpRequest * r, http_connect_cb cb ) {
	
	//warn("making connect to %s:%d", inet_ntoa( r->iaddr ), SvUV( r->port ));
	
	r->nextcb = (void *) cb;

	struct sockaddr_in iaddr;
	iaddr.sin_family      = AF_INET;
	iaddr.sin_addr.s_addr = r->iaddr.s_addr;
	iaddr.sin_port        = htons( SvUV( r->port ) );
	
	do {
		r->s = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	} while (r->s < 0 && errno == EINTR);
	
	if (r->s < 0) {
		return call_error(r, newSVpvf("Connect failed: '%s'", strerror(errno)));
	}
	
	// TODO: check return value
	fcntl(r->s, F_SETFL, O_NONBLOCK | O_RDWR);
	
	start:
	
	if (
		connect( r->s, (struct sockaddr *) &iaddr, sizeof(iaddr)) == 0
		// ||  errno == EISCONN
	) {
		//warn("connected ?");
		return on_connect_io(EV_DEFAULT, &r->io, 0);
		//return;
	} else {
		//warn("connect: %s...",strerror(errno));
		switch (errno) {
			case EINPROGRESS:
			case EALREADY:
			case EWOULDBLOCK:
				// async connect now in progress
				//client->state = CLIENT_CONNECTING;
				break;
			case EINTR:
				goto start;
			default: {
				return call_errno(r,errno);
				//strerror_r(errno, client->buffer, sizeof(client->buffer));
				//W_ERROR("connect() failed: %s (%d)", client->buffer, errno);
				//return 0;
			}
		}
	}
	
	
	//warn( "make io watcher");
	ev_io_init( &r->io, on_connect_io, r->s, EV_WRITE );
	ev_io_start( EV_DEFAULT, &r->io );
}

static void on_connect_io( struct ev_loop *loop, ev_io *w, int revents ) {
	HttpRequest *r = (HttpRequest *) w;
	
	struct sockaddr_in peer;
	socklen_t addrlen = sizeof(peer);
	
	again:
	if( getpeername( r->s, ( struct sockaddr *)&peer, &addrlen) == 0 ) {
		//warn("connected address: %s:%d",  inet_ntoa( peer.sin_addr ), ntohs( peer.sin_port ) );
		ev_io_stop( loop, w );
	} else {
		//warn("errno: %s",strerror(errno));
		switch( errno ) {
			case EINTR:
				goto again;
			case EAGAIN:
				ev_io_stop( loop, w );
				ev_io_start( loop, w );
				return;
			case ENOTCONN: {
				ev_io_stop( loop, w );
				char x[1];
				recv( r->s, x,1,0 ); // recv may give actual error
				return call_errno(r,errno);
			}
			default:
				return call_errno(r,errno);
		}
	}
	
	//warn("connect success");
	http_connect_cb cb = (http_connect_cb) r->nextcb;
	cb(r, &peer);
}




static void on_connect ( HttpRequest *r, struct sockaddr_in *peer ) {
	// struct sockaddr: res->ai_addr
	//warn("connected : %s:%d",  inet_ntoa( peer->sin_addr ), ntohs( peer->sin_port ) );
	
	// fast write without io.
	// if failed, do io
	
	STRLEN len;
	char *buf = SvPV( r->wbuf, len );
	
	size_t wr = send( r->s, buf, len, 0 );
	
	//warn ("written %d of %d", wr, len);
	
	if ( wr == len ) {
		// success
		SvREFCNT_dec( r->wbuf ); r->wbuf = 0; r->woff = 0;
		
		ev_io_init( &r->io, on_read_ready, r->s, EV_READ );
		ev_io_start( EV_DEFAULT, &r->io );
		
	}
	else
	if ( wr < 0 ) {
		debug("write failed: %s", strerror(errno));
		return call_errno(r, errno );
	}
	else
	{
		r->woff += wr;
		ev_io_init( &r->io, on_write_ready, r->s, EV_WRITE );
		ev_io_start( EV_DEFAULT, &r->io );
		return;
	}
}


/*



*/


static void on_write_ready( struct ev_loop *loop, ev_io *w, int revents ) {
	HttpRequest *r = (HttpRequest *) w;
	
	STRLEN len;
	char *buf = SvPV( r->wbuf, len );
	buf += r->woff;
	len -= r->woff;
	
	//debug("sending [%d] %s", len, buf);
	size_t wr = send( r->s, buf, len, 0 );
	//debug("wr = %d (%d)",wr, len);
	
	if ( wr == len ) {
		// success
		if (loop) ev_io_stop( loop, w );
		SvREFCNT_dec( r->wbuf ); r->wbuf = 0; r->woff = 0;
		
		ev_io_init( &r->io, on_read_ready, r->s, EV_READ );
		ev_io_start( EV_DEFAULT, &r->io );
	}
	else
	if ( wr < 0 ) {
		debug("write failed: %s", strerror(errno));
		return call_errno(r, errno );
	}
	else
	{
		r->woff += wr;
		return;
	}
}

/*

read buffer:


 [received part of buf.............]
  ^ r.rbuf: beginning of buffer
           ^ r.pos: point where parsing stopped last time
                      ^ r.ptr: pointer to end of received chunk.
                                   ^ r.end: end of allocated buffer

*/

static HV * make_headers(HttpRequest *r) {
	
	HV *h = newHV();
	SV *name = sv_2mortal( newSV(128) );
	SvUPGRADE( name, SVt_PV );
	
	SV **val;
	char *p;
	
	int rv;
	
		for (rv = 0; rv < r->http_state.header_i; rv++) {
			if( r->http_state.headers[ rv ].name.len ) {
				//SvCUR_set( name, 0 );
				//cwarn( "prepare header %-.*s", (int)r->http_state.headers[ rv ].name.len, r->http_state.headers[ rv ].name.str );
				sv_setpvn( name, r->http_state.headers[ rv ].name.str, r->http_state.headers[ rv ].name.len );
				//cwarn("header ok");
				for ( p = SvPVX(name); *p; p++ ) {
					*p = tolower(*p);
				}
				//warn("name: %s", SvPVX(name));
				if ( r->http_state.headers[rv].unique ) {
					//cwarn("unique: %-.*s: %zd", (int)SvCUR(name), SvPVX(name), r->http_state.headers[ rv ].val.len);
					(void) hv_store( h, SvPVX(name), SvCUR(name), newSVpvn( r->http_state.headers[ rv ].val.str, r->http_state.headers[ rv ].val.len ), 0 );
				} else {
					//cwarn("non unique: %-.*s", (int)SvCUR(name), SvPVX(name));
					val = hv_fetch( h, SvPVX(name), SvCUR(name), 1 );
					if (unlikely( val && *val && SvOK(*val) )) {
						SvGROW( *val, SvCUR( *val ) + 2 + r->http_state.headers[ rv ].val.len );
						sv_catpvn( *val,  "; ", 2 );
						sv_catpvn( *val, r->http_state.headers[ rv ].val.str, r->http_state.headers[ rv ].val.len );
					} else {
						hv_store( h, SvPVX(name), SvCUR(name), newSVpvn( r->http_state.headers[ rv ].val.str, r->http_state.headers[ rv ].val.len ), 0 );
					}
				}
			} else {
				val = hv_fetch( h, SvPVX(name), SvCUR(name), 1 );
				if (val && *val && SvOK( *val ) ) {
					SvGROW( *val, SvCUR( *val ) + 1 + r->http_state.headers[ rv ].val.len );
					sv_catpvn( *val,  " ", 1 );
					sv_catpvn( *val, r->http_state.headers[ rv ].val.str, r->http_state.headers[ rv ].val.len );
				}
				
			}
		}
	cwarn("done");
	return h;
}

static void maybe_call_back( HttpRequest * r, HttpRequestState state, SV *arg, int finish ) {
	switch (state) {
		case PARSE_HEADERS:
			//called when headers are done
			if (r->cb && r->cbb) {
				// have different callbacks
				if (arg) {
					call_back_cv( r->cb, 2, newRV_noinc((SV *) make_headers( r )), arg );
				} else {
					call_back_cv( r->cb, 1, newRV_noinc((SV *) make_headers( r )) );
				}
			}
			else {
				// wait for body
			}
			break;
		case RECV_BODY:
			if (r->cbb) { // have body callback
				call_back_cv( r->cbb, 1, arg );
			}
			else
			if (r->cb) {
				call_back_cv( r->cb, 2, newRV_noinc((SV *) make_headers( r )), arg );
			}
			
		default:
			croak("Wrong state");
	}
}

static void on_read_ready( struct ev_loop *loop, ev_io *w, int revents ) {
	HttpRequest *r = (HttpRequest *) w;
	size_t rd;
	//rd = recv( r->s, r->ptr, 10, 0 );
	rd = recv( r->s, r->ptr, r->end - r->ptr, 0 );
	//debug("read = %d (%s)",rd, strerror(errno));
	if ( rd > 0 ) {
		r->ptr += rd;
		*( r->ptr + 1 ) = 0;
	}
	else
	if ( rd == 0 ) {
		debug("eof");
		// TODO
	}
	else {
		
	}
	//warn("read: %d ",rd);
	
	//r->parse.state = start;
	int rv;
	cwarn("enter read (%d)", r->state);
	if (r->state == NONE) {
		parse_http_reset( &r->http_state );
		r->state = PARSE_LINE;
	}
	
	if (r->state == PARSE_LINE) {
		r->state = PARSE_LINE;
		r->http_state.p = r->pos;
		r->http_state.e = r->ptr;
		
		rv = parse_http_response_line( &r->http_state );
		r->pos = r->http_state.p;
		if (unlikely( rv <= 0 )) {
			cwarn("failed to parse response line: %d", rv);
			if (rv < 0) return;
			ev_io_stop( loop, w );
			croak("TODO: How to handle error?");
		}
		cwarn("parsed: HTTPV: %0.1f (%d)", (double)rv/10, rv);
		r->state = PARSE_HEADERS;
	}
	
	if (r->state == PARSE_HEADERS) {
		r->http_state.p = r->pos;
		r->http_state.e = r->ptr;
		
		rv = parse_http_headers( &r->http_state );
		r->pos = r->http_state.p;
		
		if (unlikely( rv <= 0 )) {
			if (rv < 0) return;
			ev_io_stop( loop, w );
			croak("TODO: How to handle error?");
		}
		cwarn("parsed headers: %d; clength: %zd", rv, r->http_state.content_length);
		cwarn("all: %s", r->rbuf);
		
		maybe_call_back(r, PARSE_HEADERS, 0, 0);
		
		r->state = RECV_BODY;
		
	}
	
	cwarn("Next: %-.10s", r->pos);
	
	if (r->state == RECV_BODY) {
		if (r->http_state.content_length > 0) {
			if ( r->http_state.content_length <= r->ptr - r->pos ) {
				cwarn("Have enough content");
				ev_io_stop( loop, w );
				
				errno = 0;
				debug("before cb=%p", r->cb);
				cwarn("call back. %zd headers, %zd body", r->http_state.header_i, r->http_state.content_length);
				//make_headers( r );
				
				maybe_call_back(r, RECV_BODY, newSVpvn( r->pos, r->http_state.content_length ), 1);
				
				//call_back(r, 2, newRV_noinc((SV *) make_headers( r )), newSVpvn( r->pos, r->http_state.content_length ) );
				
				r->state = NONE;
				
				if (r->http_state.connection == connection_close) {
					debug( "closing s");
					close(r->s);
					r->s = -1;
				}
				
				if( r->http_state.content_length < r->ptr - r->pos ) {
					debug("Have trailing: [%zd] %s",(r->ptr - r->pos) - r->http_state.content_length, r->pos + r->http_state.content_length);
				}
			} else {
				debug("Have not enough content. Need %zd, have: %zd", r->http_state.content_length , r->ptr - r->pos);
				return;
			}
		}
		else
		if (r->http_state.transfer_encoding == encoding_chunked) {
			cwarn("received chunked");
			char *lf;
			if (lf = strchr(r->pos, LF)) {
				warn("have length: %-.5s (%p)", r->pos, r->pos);
				unsigned long int length = strtoul( r->pos, 0, 16 );
				r->pos = lf+1;
				if ( r->ptr - r->pos < length + 1 ) {
					// chunk-size [ chunk-extension ] CRLF
					// but forgive only LF
					return; // need more data
				}
				warn("have length: %x next: %s", length, r->pos);
				r->pos += length;
				if ( ( r->ptr - r->pos > 1 && *r->pos == CR && *(r->pos + 1) == LF ) ) {
					r->pos+=2;
				}
				else
				if ( r->ptr - r->pos > 0 && *r->pos == LF ) {
					r->pos++;
				}
				//complete chunk
				
TODO: Don't shift pos by length
				
				//fast check for 0
				if ( r->ptr - r->pos > 2 && *r->pos == '0' && *(r->pos + 1) == CR && *(r->pos + 2) == LF ) ) {
					
					maybe_call_back(r, RECV_BODY, newSVpvn( r->pos, length ), 0);
					r->pos +
				}
				else
				if ( r->ptr - r->pos > 1 && *r->pos == '0' && *(r->pos + 1) == LF ) ) {
					
				}
				else {
					
				}
				
				if (lf = strchr(r->pos, LF) && lf > r->pos) {
					unsigned long int nextlen = strtoul( r->pos, 0, 16 );
					if (nextlen == 0) {
						
					} else {
						
					}
				}
				
				//if (  )
				warn("have next: %s", r->pos);
				//maybe_call_back(r, RECV_BODY, newSVpvn( r->pos, length ), 0);
				
			} else {
				return;
			}
		}
	}
	if (r->state != NONE) {
		croak("Shit! Wrong state");
	}
	
	// finishing request
	if (r->cb)       { SvREFCNT_dec(r->cb);   r->cb = 0;   }
	if( r->wbuf )    { SvREFCNT_dec(r->wbuf); r->wbuf = 0; }
	
	
	if (av_len( r->requests ) > -1) {
		SV *next = av_shift( r->requests );
		AV *req  = (AV *) SvRV( next );
		SV *svbuf = *av_fetch( req,0,0 );
		SV *cb    = *av_fetch( req,1,0 );
		
		cwarn("make new request %-.*s", str2toeol( SvPVX( svbuf ) ));
		
		SvREFCNT_inc(r->cb  = cb);
		SvREFCNT_inc(r->wbuf = svbuf);
		
		
		r->pos  = r->ptr = r->rbuf;
		r->woff = 0;
		if (r->s > -1) {
			on_write_ready(EV_DEFAULT, &r->io, 0);
		} else {
			http_connect(r, on_connect);
		}
	}
}

#define unset_sv(x) STMT_START { SvREFCNT_dec(x); x = 0; } STMT_END

MODULE = EV::HTTP::Client		PACKAGE = EV::HTTP::Client

BOOT:
{
    I_EV_API ("EV::HTTP::Client");
}

SV *
new(SV *pk, HV * conf)
CODE:
	HV *stash = gv_stashpv(SvPV_nolen(pk), TRUE);
	HttpRequest * r = safemalloc( sizeof(HttpRequest) );
	if (!r) croak("Can't allocate request");
	memset(r,0,sizeof(HttpRequest));
	
	STRLEN rlen = 128 * 1024;
	
	r->rbuf = safemalloc( rlen );
	if (!r->rbuf) {
		safefree(r);
		croak("Can't allocate buffer");
	}
	r->pos = r->ptr = r->rbuf;
	r->end = r->rbuf + rlen;
	r->requests = newAV();
	
	r->http_state.header_max = 128;
	r->http_state.headers = safemalloc( sizeof( header_t ) * r->http_state.header_max );
	if (!r->http_state.headers) {
		safefree(r->rbuf);
		safefree(r);
		croak("Can't allocate memory for headers");
	}
	
	
	SV **key;
	if ((key = hv_fetchs(conf, "host", 0)) && SvOK(*key)) {
		r->host = *key;
		SvREFCNT_inc(r->host);
	}
	if ((key = hv_fetchs(conf, "port", 0)) && SvOK(*key)) {
		r->port = *key;
		SvREFCNT_inc(r->port);
	} else {
		r->port = newSViv(80);
		SvREFCNT_inc(r->port);
	}
	
	r->state = NONE;
	ST(0) = sv_2mortal (sv_bless (newRV_noinc (newSViv(PTR2IV( r ))), stash));
	XSRETURN(1);

void
DESTROY(SV *self)
CODE:
	HttpRequest * r = ( HttpRequest * ) SvUV( SvRV( self ) );
	
	if (r->host)      { SvREFCNT_dec(r->host); }
	if (r->port)      { SvREFCNT_dec(r->port); }
	if (r->cb)        { SvREFCNT_dec(r->cb); }
	if( r->wbuf )     { SvREFCNT_dec(r->wbuf); }
	if( r->requests ) { SvREFCNT_dec(r->requests); }
	
	warn("destroying request");
	safefree(r->http_state.headers);
	safefree(r->rbuf);
	safefree(r);

void
parse_headers( SV * svbuf )
PROTOTYPE: $
CODE:
	int rv;
	parse_http_state s;
	memset(&s,0,sizeof(s));

	header_t headers[s.header_max = 128]; // headers: header_t*
	header_t *h = headers;
	memset(headers,0,sizeof(headers));
	
	s.headers = headers;

	STRLEN len;
	s.p = SvPV(svbuf,len);
	s.e = s.p + len;
	
	rv = parse_http_response_line(&s);
	rv = parse_http_headers(&s);
	
	HV *r = newHV();
	SV *name = sv_2mortal( newSV(128) );
	SvUPGRADE( name, SVt_PV );
	
	SV **val;
	char *p;
	
	if (rv > 0) {
		for (rv = 0; rv < s.header_i; rv++) {
			if( s.headers[ rv ].name.len ) {
				SvCUR_set( name, 0 );
				sv_catpvn( name, s.headers[ rv ].name.str, s.headers[ rv ].name.len );
				for ( p = SvPVX(name); *p; p++ ) {
					*p = tolower(*p);
				}
				//warn("name: %s", SvPVX(name));
				if ( s.headers[rv].unique ) {
					//cwarn("unique: %-.*s", last->name.len, last->name.str);
					(void) hv_store( r, SvPVX(name), SvCUR(name), newSVpvn( s.headers[ rv ].val.str, s.headers[ rv ].val.len ), 0 );
				} else {
					//cwarn("non unique: %-.*s", last->name.len, last->name.str);
					val = hv_fetch( r, SvPVX(name), SvCUR(name), 1 );
					if (unlikely( val && *val && SvOK(*val) )) {
						SvGROW( *val, SvCUR( *val ) + 2 + s.headers[ rv ].val.len );
						sv_catpvn( *val,  "; ", 2 );
						sv_catpvn( *val, s.headers[ rv ].val.str, s.headers[ rv ].val.len );
					} else {
						hv_store( r, SvPVX(name), SvCUR(name), newSVpvn( s.headers[ rv ].val.str, s.headers[ rv ].val.len ), 0 );
					}
				}
			} else {
				val = hv_fetch( r, SvPVX(name), SvCUR(name), 1 );
				if (val && *val && SvOK( *val ) ) {
					SvGROW( *val, SvCUR( *val ) + 1 + s.headers[ rv ].val.len );
					sv_catpvn( *val,  " ", 1 );
					sv_catpvn( *val, s.headers[ rv ].val.str, s.headers[ rv ].val.len );
				}
				
			}
		}
	}
	ST(0) = sv_2mortal( newRV_noinc( (SV *) r ) );
	XSRETURN(1);
	
	/*
	XSRETURN_UNDEF;
	if (result < 0) {
		ST(0) = sv_2mortal( newSViv( 0 ) );
		XSRETURN(1);
	}
	else
	if (result != 0) {
		EXTEND(SP,2);
		ST(0) = UNDEF;
		ST(1) = r->error ? r->error : sv_2mortal(newSVpvs("No error stored :("));
		XSRETURN(2);
	}
	else {
		ST(0) = sv_2mortal( newRV_noinc( (SV *) r->headers ) );
		XSRETURN(1);
	}
	*/
	


void
request(SV *self, SV *svbuf, SV *cb, ...)
PROTOTYPE: $$$;$
CODE:
	HttpRequest * r = ( HttpRequest * ) SvUV( SvRV( self ) );
	r->count++;
	warn("Performing request [%d via %d] to %s:%"UVf"\n%s\n", r->count, r->s, SvPV_nolen(r->host), SvUV( r->port ), SvPV_nolen(svbuf));
	
	/*
		resolve -> connect -> write -> read
		
	*/
	
	//r->cb = cb;
	
	
	//http_resolve(r, on_resolve);
	
	if ( r->s > 0 )  {
		warn("push request into queue");
		AV *req = newAV();
		SvREFCNT_inc( svbuf  );
		SvREFCNT_inc( cb );
		//warn ("created array, refcnt=%d[%d, %d]", SvREFCNT( req ), SvREFCNT(svbuf), SvREFCNT(cb));
		av_push( req, svbuf );
		av_push( req, cb );
		
		if (items > 3) {
			SvREFCNT_inc( ST(3) );
			av_push( req, ST(3) );
		}
		av_push( r->requests, newRV_noinc( (SV *) req ) );
		
		//warn ("pushed array, refcnt=%d (main:%d)", SvREFCNT( req ), SvREFCNT( r->requests ));
	} else {
		//warn("perform request");
		
		SvREFCNT_inc( r->cb = cb );
		SvREFCNT_inc( r->wbuf = svbuf );
		if (items > 3) {
			SvREFCNT_inc( r->cbb = ST(3) );
		}
		
		struct in_addr ip;
		if ( inet_aton( SvPV_nolen(r->host), &ip ) ) {
			r->iaddr.s_addr = ip.s_addr;
			//warn ("resolved: (%s)", inet_ntoa( r->iaddr ));
			http_connect(r, on_connect);
		} else {
			croak("IP required");
		}
	}
	
	
	XSRETURN_UNDEF;



void
http_request(host, port, req, ...)
	char *host;
	U16   port;
	char *req;
	PROTOTYPE: $$$@
	CODE:
		warn("call %s:%d -> %s", host,port,req);
		
		/*
		Config config;
		memset(&config, 0, sizeof(Config));
		Worker worker;
		memset(&worker, 0, sizeof(Worker));
		worker.config = &config;
		worker.loop = ev_default_loop(0);

		config.concur_count = 1;
		config.req_count = 10;
		config.saddr = saddr;
		
		Client *client = client_new( &worker );
		
		client_state_machine(client);
		*/
		
		//connect, send request, read and parse response
		
		//url_t url;

		//parse_url(&url, urlc);
		//warn("call sc<%s> host<%s> port<%d>", url.scheme, url.host, url.port, url.path, url.query, url.fragment, url.username, url.password);
		warn("returning");
		
		XSRETURN_UNDEF;


