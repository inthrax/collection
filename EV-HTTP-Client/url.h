#ifndef _URL_H
#define _URL_H

typedef struct {
    char *scheme;               /* mandatory */
    char *host;                 /* mandatory */
    uint16_t port;              /* optional */
    char *path;                 /* optional */
    char *query;                /* optional */
    char *fragment;             /* optional */
    char *username;             /* optional */
    char *password;             /* optional */
} url_t;

//+url_t * parse_url(const char *);
int parse_url(url_t *purl, const char *url) {



#endif