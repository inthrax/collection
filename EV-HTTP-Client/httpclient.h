/*
 * weighttp - a lightweight and simple webserver benchmarking tool
 *
 * Author:
 *     Copyright (c) 2009-2011 Thomas Porzelt
 *
 * License:
 *     MIT, see COPYING file
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <errno.h>
#include <string.h>

#include <unistd.h>
#include <stdint.h>
#include <fcntl.h>
#include <inttypes.h>
#include <sys/socket.h>
#include <netdb.h>

#include <ev.h>
#include <pthread.h>

#define CLIENT_BUFFER_SIZE 32 * 1024

#define W_MALLOC(t, n) ((t*) calloc((n), sizeof(t)))
#define W_REALLOC(p, t, n) ((t*) realloc(p, (n) * sizeof(t)))
#define W_ERROR(f, ...) fprintf(stderr, "error: " f "\n", __VA_ARGS__)
#define UNUSED(x) ( (void)(x) )

struct Config;
typedef struct Config Config;
struct Stats;
typedef struct Stats Stats;
struct Worker;
typedef struct Worker Worker;
struct Client;
typedef struct Client Client;

struct Config {
	uint64_t req_count;
	uint8_t thread_count;
	uint16_t concur_count;
	uint8_t keep_alive;

	char *request;
	uint32_t request_size;
	struct addrinfo *saddr;
};


struct Stats {
	ev_tstamp req_ts_min;	/* minimum time taken for a request */
	ev_tstamp req_ts_max;	/* maximum time taken for a request */
	ev_tstamp req_ts_total;	/* total time taken for all requests (this is not ts_end - ts_start!) */
	uint64_t req_todo;		/* total number of requests to do */
	uint64_t req_started;	/* total number of requests started */
	uint64_t req_done;		/* total number of requests done */
	uint64_t req_success;	/* total number of successful requests */
	uint64_t req_failed;	/* total number of failed requests */
	uint64_t req_error;		/* total number of error'd requests */
	uint64_t bytes_total;	/* total number of bytes received (html+body) */
	uint64_t bytes_body;	/* total number of bytes received (body) */
	uint64_t req_1xx;
	uint64_t req_2xx;
	uint64_t req_3xx;
	uint64_t req_4xx;
	uint64_t req_5xx;
};

struct Worker {
	uint8_t id;
	Config          *config;
	struct ev_loop  *loop;
	char            *request;
	Client         **clients;
	uint16_t         num_clients;
	Stats            stats;
	uint64_t         progress_interval;
};


struct Client {
	enum {
		CLIENT_START,
		CLIENT_CONNECTING,
		CLIENT_WRITING,
		CLIENT_READING,
		CLIENT_ERROR,
		CLIENT_END
	} state;

	enum {
		PARSER_START,
		PARSER_HEADER,
		PARSER_BODY
	} parser_state;
	
	Worker *worker;
	//struct ev_loop  *loop;
	
	ev_io sock_watcher;
	
	uint32_t buffer_offset;
	uint32_t parser_offset;
	uint32_t request_offset;
	ev_tstamp ts_start;
	ev_tstamp ts_end;
	uint8_t keepalive;
	uint8_t success;
	uint8_t status_success;
	uint8_t chunked;
	int64_t chunk_size;
	int64_t chunk_received;
	int64_t content_length;
	uint64_t bytes_received; /* including http header */
	uint16_t header_size;

	char buffer[CLIENT_BUFFER_SIZE];
};


uint64_t str_to_uint64(char *str);
Client *client_new(Worker *worker);
void client_free(Client *client);
void client_state_machine(Client *client);

